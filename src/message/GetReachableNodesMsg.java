package message;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import structure.Edge;
import akka.actor.ActorPath;
import akka.actor.ActorRef;

public class GetReachableNodesMsg implements Serializable{

public GetReachableNodesMsg(HashSet reachableNodes,
			Hashtable neighborsOfReachableNodes,
			Hashtable corenessOfNeighborsOfReachableNodes) {
		super();
		this.reachableNodes = reachableNodes;
		this.neighborsOfReachableNodes = neighborsOfReachableNodes;
		this.corenessOfNeighborsOfReachableNodes = corenessOfNeighborsOfReachableNodes;
	}

HashSet reachableNodes;
Hashtable neighborsOfReachableNodes;
Hashtable corenessOfNeighborsOfReachableNodes;
	private Edge edge;
	private int startNode;

	private int currentPartition;
	public GetReachableNodesMsg(Edge edge,int node, int currentPartition) {
		this.edge = edge;
		this.startNode = node;
		this.currentPartition= currentPartition;
	}

	public Edge getEdge() {
		return edge;
	}

	public int getStartNode() {
		return startNode;
	}

	public int getCurrentPartition() {
		return currentPartition;
	}

	public void setCurrentPartition(int currentPartition) {
		this.currentPartition = currentPartition;
	}

	public HashSet getReachableNodes() {
		return reachableNodes;
	}

	public void setReachableNodes(HashSet reachableNodes) {
		this.reachableNodes = reachableNodes;
	}

	public Hashtable getNeighborsOfReachableNodes() {
		return neighborsOfReachableNodes;
	}

	public void setNeighborsOfReachableNodes(Hashtable neighborsOfReachableNodes) {
		this.neighborsOfReachableNodes = neighborsOfReachableNodes;
	}

	public Hashtable getCorenessOfNeighborsOfReachableNodes() {
		return corenessOfNeighborsOfReachableNodes;
	}

	public void setCorenessOfNeighborsOfReachableNodes(
			Hashtable corenessOfNeighborsOfReachableNodes) {
		this.corenessOfNeighborsOfReachableNodes = corenessOfNeighborsOfReachableNodes;
	}

}
