package message;

import java.io.Serializable;

import akka.actor.ActorPath;
import akka.actor.ActorRef;

public class DistantKCoreResultMsg implements Serializable{

	private final int partition;
	private final ActorRef path;

	public DistantKCoreResultMsg(int partition,ActorRef path) {
		this.partition = partition;
		this.path = path;
	}

	public int getPartition() {
		return partition;
	}

	public ActorRef getPath() {
		return path;
	}
}
