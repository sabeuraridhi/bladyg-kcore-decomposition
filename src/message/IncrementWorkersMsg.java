package message;

import java.io.Serializable;

public class IncrementWorkersMsg implements Serializable{
public IncrementWorkersMsg(int nbIncrementedWorkers) {
		super();
		this.nbIncrementedWorkers = nbIncrementedWorkers;
	}

int nbIncrementedWorkers;

public int getNbIncrementedWorkers() {
	return nbIncrementedWorkers;
}

public void setNbIncrementedWorkers(int nbIncrementedWorkers) {
	this.nbIncrementedWorkers = nbIncrementedWorkers;
}

}
