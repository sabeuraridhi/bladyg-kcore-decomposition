package message;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

import structure.IntArray;

public class PropagateReachableNodesW2WCase2Msg implements Serializable{
public PropagateReachableNodesW2WCase2Msg(int node) {
		super();
		this.node = node;
	}
int node;
Hashtable neighborsOfReachableNodes;
Vector reachableNodes;
public int getNode() {
	return node;
}
public void setNode(int node) {
	this.node = node;
	neighborsOfReachableNodes=new Hashtable<Integer,IntArray>();
	reachableNodes=new Vector();
	
}
public Hashtable getNeighborsOfReachableNodes() {
	return neighborsOfReachableNodes;
}
public void setNeighborsOfReachableNodes(Hashtable neighborsOfReachableNodes) {
	this.neighborsOfReachableNodes = neighborsOfReachableNodes;
}
public Vector getReachableNodes() {
	return reachableNodes;
}
public void setReachableNodes(Vector reachableNodes) {
	this.reachableNodes = reachableNodes;
}


}
