package message;

import java.io.Serializable;

import structure.Edge;

public class RemoteKCoreUpdateMsg implements Serializable{
private int currentPartition;
private  int step;
private  int partition;
private  int p1;
private  int p2;
private  Edge edge;
private int defaultNode;

public RemoteKCoreUpdateMsg(int step, int partition)
{
	this.step = step;
	this.partition = partition;
}
public RemoteKCoreUpdateMsg(int step, Edge e)
{
	this.step = step;
	this.edge = e;
}
public RemoteKCoreUpdateMsg(int step, Edge e, int defaultN)
{
	this.step = step;
	this.edge = e;
	this.defaultNode=defaultN;
}
public RemoteKCoreUpdateMsg(int step, int p1, int p2)
{
	this.step = step;
	this.p1 = p1;
	this.p2 = p2;
}

public int getStep() {
return step;
}

public int getPartition() {
return partition;
}
public Edge getEdge() {
	return edge;
}
public void setEdge(Edge edge) {
	this.edge = edge;
}
public int getDefaultNode() {
	return defaultNode;
}
public void setDefaultNode(int defaultNode) {
	this.defaultNode = defaultNode;
}
public int getCurrentPartition() {
	return currentPartition;
}
public void setCurrentPartition(int currentPartition) {
	this.currentPartition = currentPartition;
}


}

