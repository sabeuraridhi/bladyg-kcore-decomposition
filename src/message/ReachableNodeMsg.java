package message;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

import structure.Edge;
import akka.actor.ActorPath;
import akka.actor.ActorRef;

public class ReachableNodeMsg implements Serializable{

	private  int node;
	private  int coreness;
	private  Hashtable corenessOfNeighbors;

	public ReachableNodeMsg(int node,int coreness) {
		this.node = node;
		this.coreness = coreness;
		corenessOfNeighbors=new Hashtable<Integer, Integer>();
	}

	public int getNode() {
		return node;
	}

	public int getCoreness() {
		return coreness;
	}

	public void setNode(int node) {
		this.node = node;
	}

	public void setCoreness(int coreness) {
		this.coreness = coreness;
	}

	public Hashtable getCorenessOfNeighbors() {
		return corenessOfNeighbors;
	}

	public void setCorenessOfNeighbors(Hashtable corenessOfNeighbors) {
		this.corenessOfNeighbors = corenessOfNeighbors;
	}

	public void showNode()
	{
		
		
		Enumeration hkeys=corenessOfNeighbors.keys();
		System.out.println("The reachable Node: "+node+" have coreness = "+coreness + " and have neighbors: " );
		while (hkeys.hasMoreElements())
			System.out.println("Neighbor "+corenessOfNeighbors.get((int)hkeys.nextElement())+" ");
		
	}
}
