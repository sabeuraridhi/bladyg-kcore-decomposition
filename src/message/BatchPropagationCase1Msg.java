package message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class BatchPropagationCase1Msg implements Serializable {
    public BatchPropagationCase1Msg(
			ArrayList<PropagateReachableNodesW2WCase1Msg> arrayList) {
		super();
		this.listOfMessage1 = arrayList;
	}

	ArrayList<PropagateReachableNodesW2WCase1Msg> listOfMessage1;

	public ArrayList<PropagateReachableNodesW2WCase1Msg> getListOfMessage1() {
		return listOfMessage1;
	}

	public void setListOfMessage1(
			ArrayList<PropagateReachableNodesW2WCase1Msg> listOfMessage1) {
		this.listOfMessage1 = listOfMessage1;
	}
    

}
