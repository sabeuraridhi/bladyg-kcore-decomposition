package message;

import java.io.Serializable;

import akka.actor.ActorRef;

public class CorenessOfNodeMsg implements Serializable{
	
public CorenessOfNodeMsg(int node, ActorRef master) {
		super();
		this.node = node;
		this.master=master;
	}

int node;
ActorRef master;
public int getNode() {
	return node;
}

public void setNode(int node) {
	this.node = node;
}

public ActorRef getMaster() {
	return master;
}

public void setMaster(ActorRef master) {
	this.master = master;
}

}
