package message;

import java.io.Serializable;
import java.util.Hashtable;

import akka.actor.ActorRef;

public class DistantUpdateMsg implements Serializable{
	private final Hashtable distCoreness;
	private final ActorRef MasterActor;
	private final int partition;
	private final int typeEdge;
	
	public DistantUpdateMsg(Hashtable distCoreness, ActorRef masterActor,int partition,int typeEdge) {
		this.partition=partition;
		this.distCoreness = distCoreness;
		MasterActor = masterActor;
		this.typeEdge=typeEdge;
	}
	public Hashtable getDistCoreness() {
		return distCoreness;
	}
	public ActorRef getMasterActor() {
		return MasterActor;
	}
	public int getPartition() {
		return partition;
	}
	public int getTypeEdge() {
		return typeEdge;
	}

	
	

}

