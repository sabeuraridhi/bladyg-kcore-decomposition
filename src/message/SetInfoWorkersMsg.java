package message;

import java.io.Serializable;
import java.util.Hashtable;

import akka.actor.ActorRef;

public class SetInfoWorkersMsg implements Serializable{
	public SetInfoWorkersMsg(Hashtable<Integer, ActorRef> infoWorkersInWorker, int partition) {
		super();
		InfoWorkersInWorker = infoWorkersInWorker;
		this.partition=  partition;
	}

	Hashtable<Integer, ActorRef>  InfoWorkersInWorker;
	int partition; 
	public Hashtable<Integer, ActorRef> getInfoWorkersInWorker() {
		return InfoWorkersInWorker;
	}

	public void setInfoWorkersInWorker(
			Hashtable<Integer, ActorRef> infoWorkersInWorker) {
		InfoWorkersInWorker = infoWorkersInWorker;
	}

	public int getPartition() {
		return partition;
	}

	public void setPartition(int partition) {
		this.partition = partition;
	}
}
