package message;

import java.io.Serializable;

public class UpdateNeighborOfOneNodeMsg implements Serializable{
public UpdateNeighborOfOneNodeMsg(int node, int neighbor,
			int corenessNeighb, int partitionNeighb, int typeEdge) {
		super();
		this.node = node;
		this.neighbor = neighbor;
		this.corenessNeighb = corenessNeighb;
		this.partitionNeighb = partitionNeighb;
		this.typeEdge=typeEdge;
	}
/*
public UpdateNeighborOfOneNodeMsg(int node, int neighbor, int corenessNeighb) {
		super();
		this.node = node;
		this.neighbor = neighbor;
		this.corenessNeighb = corenessNeighb;
		
	}
	*/
int node;
int neighbor;
int corenessNeighb;
int partitionNeighb;
int typeEdge;
public int getNode() {
	return node;
}
public void setNode(int node) {
	this.node = node;
}
public int getNeighbor() {
	return neighbor;
}
public void setNeighbor(int neighbor) {
	this.neighbor = neighbor;
}
public int getCorenessNeighb() {
	return corenessNeighb;
}
public void setCorenessNeighb(int corenessNeighb) {
	this.corenessNeighb = corenessNeighb;
}
public int getPartitionNeighb() {
	return partitionNeighb;
}
public void setPartitionNeighb(int partitionNeighb) {
	this.partitionNeighb = partitionNeighb;
}
public int getTypeEdge() {
	return typeEdge;
}
public void setTypeEdge(int typeEdge) {
	this.typeEdge = typeEdge;
}
}
