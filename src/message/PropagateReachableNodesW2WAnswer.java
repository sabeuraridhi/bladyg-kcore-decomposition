package message;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Vector;

import structure.IntArray;

public class PropagateReachableNodesW2WAnswer implements Serializable{

	
	public PropagateReachableNodesW2WAnswer(HashSet reachableNodes,
			Hashtable neighborsOfReachableNodes,
			Hashtable corenessOfNeighborsOfReachableNodes) {
		super();
		this.reachableNodes = reachableNodes;
		this.neighborsOfReachableNodes = neighborsOfReachableNodes;
		this.corenessOfNeighborsOfReachableNodes = corenessOfNeighborsOfReachableNodes;
	}

	HashSet reachableNodes;
Hashtable neighborsOfReachableNodes;
Hashtable corenessOfNeighborsOfReachableNodes;

public Hashtable getNeighborsOfReachableNodes() {
	return neighborsOfReachableNodes;
}
public void setNeighborsOfReachableNodes(Hashtable neighborsOfReachableNodes) {
	this.neighborsOfReachableNodes = neighborsOfReachableNodes;
}
public HashSet getReachableNodes() {
	return reachableNodes;
}
public void setReachableNodes(HashSet reachableNodes) {
	this.reachableNodes = reachableNodes;
}
public Hashtable getCorenessOfNeighborsOfReachableNodes() {
	return corenessOfNeighborsOfReachableNodes;
}
public void setCorenessOfNeighborsOfReachableNodes(
		Hashtable corenessOfNeighborsOfReachableNodes) {
	this.corenessOfNeighborsOfReachableNodes = corenessOfNeighborsOfReachableNodes;
}


}
