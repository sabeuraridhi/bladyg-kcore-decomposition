package message;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Vector;

import structure.Edge;
import akka.actor.ActorPath;
import akka.actor.ActorRef;

public class FrontierEdgeWithReacheableNodesMsg implements Serializable{

	private final Edge edge;
	private final ActorRef distantActor;
	private final Vector reacheableNodes;
	private final Hashtable NeighborsOfReacheableNodes;
	private final Hashtable CorenessOfNeighborsOfReacheableNodes;

	public FrontierEdgeWithReacheableNodesMsg(Edge edge,Vector reacheableNodes, Hashtable h,Hashtable CorenessOfNeighborsOfReacheableNodes,ActorRef distantActor) {
		this.edge = edge;
		this.reacheableNodes = reacheableNodes;
		this.NeighborsOfReacheableNodes = h;
		this.CorenessOfNeighborsOfReacheableNodes=CorenessOfNeighborsOfReacheableNodes;
		this.distantActor=distantActor;	

	}

	public Edge getEdge() {
		return edge;
	}

	public Vector getreacheableNodes() {
		return reacheableNodes;
	}

	public Hashtable getNeighborsOfReacheableNodes() {
		return NeighborsOfReacheableNodes;
	}

	public Vector getReacheableNodes() {
		return reacheableNodes;
	}

	public Hashtable getCorenessOfNeighborsOfReacheableNodes() {
		return CorenessOfNeighborsOfReacheableNodes;
	}

	public ActorRef getDistantActor() {
		return distantActor;
	}

}
