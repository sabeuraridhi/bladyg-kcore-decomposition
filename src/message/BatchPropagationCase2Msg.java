package message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class BatchPropagationCase2Msg implements Serializable {
    public BatchPropagationCase2Msg(ArrayList<PropagateReachableNodesW2WCase2Msg> arrayList) {
		super();
		this.listOfMessage2 = arrayList;
	}

	ArrayList<PropagateReachableNodesW2WCase2Msg> listOfMessage2;

	public ArrayList<PropagateReachableNodesW2WCase2Msg> getListOfMessage2() {
		return listOfMessage2;
	}

	public void setListOfMessage2(
			ArrayList<PropagateReachableNodesW2WCase2Msg> listOfMessage2) {
		this.listOfMessage2 = listOfMessage2;
	}
    

}
