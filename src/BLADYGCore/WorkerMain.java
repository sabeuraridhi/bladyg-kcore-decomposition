package BLADYGCore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import akka.cluster.Cluster;
import akka.routing.FromConfig;
import akka.routing.RoundRobinRouter;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class WorkerMain {
	
	
	// This class should be executed in every salve machine
	public static void main(String[] args) throws IOException {
		
	  //Check the parameters	  
	  if(args.length==3)
		 {
		  
		  		final String indPort = args[0];
		    	final String clusterFile = args[1];
			    final int nbWorkers = Integer.parseInt(args[2]);		 
				int port = Integer.parseInt(indPort)+2550;
				//  int port = 2552;
				
				String hostname = WorkerMain.getWorkerIP(clusterFile,Integer.parseInt(args[0]));
				String seedNodes=WorkerMain.getSeedNodesConf(clusterFile);
				
				/*
				if (hostname.equals(""))
				{
					port = 0;
					hostname="127.0.0.1";
					seedNodes ="[\"akka.tcp://kCoreDescompositionSystem@127.0.0.1:2552\"]";
				}
				*/
				System.out.println("[Worker] Hostname = "+hostname+" port "+port);
				System.out.println("[Worker] Seed nodes = "+seedNodes);
				
				//Creation of a configuration object in the case of a real cluster 

			    final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
			  	      withFallback(ConfigFactory.parseString("akka.cluster.roles = [workerRole]")).
			  	      withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname = \""+hostname+"\"")).
			  	    withFallback(ConfigFactory.parseString("akka.cluster.role.workerRole.min-nr-of-members = "+ nbWorkers )).
			  		withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = "+seedNodes)).
			  	      withFallback(ConfigFactory.load("kcore"));
			    
				//Creation of a configuration object in the case of a local execution of BLADYG 
			    
			    final Config configLocal = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
			    		withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname = \"127.0.0.1\"")).
			    		withFallback(ConfigFactory.parseString("akka.cluster.role.workerRole.min-nr-of-members = "+ nbWorkers )).
			    		withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://kCoreDescompositionSystem@127.0.0.1:2552\"]").
			    	      withFallback(ConfigFactory.parseString("akka.cluster.roles = [workerRole]")).
			    	      withFallback(ConfigFactory.load("kcore")));
			    
			    //Creation of an Actor System that corresponds to BLADYG solution

			    final ActorSystem system = ActorSystem.create("kCoreDescompositionSystem", configLocal); 
			
			    //Register the current actor as a Worker 
			    Cluster.get(system).registerOnMemberUp(new Runnable() {
			      @Override
			      public void run() {
			    	  system.actorOf(Props.create(Worker.class),"worker");
			      }
			    });
			  }
	  else
		 {
			 System.out.println("Check your parameters on Worker side");
		 }
  		}
	public static String getWorkerIP(String FileIPs, int ind) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(FileIPs));
		String line="";
		String ip="";
		while((line=reader.readLine())!=null)
		{
			
			if(!line.equals(""))
			{
				String[] parts = line.split("\\s");
				if((parts[0].toUpperCase().equals("WORKER"))&&(Integer.parseInt(parts[1])==ind))
					ip= parts[2]; 

			}
		}
		
		reader.close();
		return ip;
	}
	public static String getSeedNodesConf(String FileIPs) throws IOException
	{
		BufferedReader reader = new BufferedReader(new FileReader(FileIPs));
		String line="";
		String seed="[";
		int port=0;
		while((line=reader.readLine())!=null)
		{
			if(!line.equals(""))
			{
				String[] parts = line.split("\\s");
				seed+="\"akka.tcp://kCoreDescompositionSystem@"+parts[2]+":"+2552+"\","; 
			}
		}
		seed = seed.substring(0, seed.length()-1); 
		seed+="]";
		reader.close();
		return seed;
	}
  
}
