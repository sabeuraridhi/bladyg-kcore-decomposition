package BLADYGCore;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import structure.Edge;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.Cluster;

public class MasterMain {

	// This class should be executed in the master machine 
  public static void main(String[] args) throws IOException {
   
    //check the parameters 
	 if(args.length==8)
	 {
		final String graphFile = args[0];
	    final String partFile = args[1];
	    final String partMethod = args[2];
	    final int nbWorkers = Integer.parseInt(args[3]);
	    final int nbPartitions = Integer.parseInt(args[4]);
	    final int nbInsertions = Integer.parseInt(args[5]);
	    final int nbDeletions = Integer.parseInt(args[5]);
	    final String TypeOfEdges = args[6];
	    final String clusterFile = args[7];
	    
	    int port = 2552;
	    
	    
	    String hostname = MasterMain.getMasterIP(clusterFile);
		String seedNodes=MasterMain.getSeedNodesConf(clusterFile);
		/*
		if (hostname.equals(""))
		{
			port = 0;
			hostname="127.0.0.1";
			seedNodes ="[\"akka.tcp://kCoreDescompositionSystem@127.0.0.1:2552\"]";
		}
		*/
		System.out.println("[Master] Hostname = "+hostname+" port "+port);
		System.out.println("[Master] Seed nodes = "+seedNodes);
		
	    
		//Creation of a configuration object in the case of a real cluster 
	    final Config config = ConfigFactory.parseString("akka.cluster.roles = [masterRole]  ").
	    		withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port="+port )).
	    		withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname = \""+hostname+"\"")).
	    		withFallback(ConfigFactory.parseString("akka.cluster.role.workerRole.min-nr-of-members = " +nbWorkers)).
	    		withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = "+seedNodes).
	    		withFallback(ConfigFactory.load("kcore")));
	    
	    
	    
		//Creation of a configuration object in the case of a local execution of BLADYG 

	    final Config configLocal = ConfigFactory.parseString("akka.cluster.roles = [masterRole] ").
	    		withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0" )).
	    		withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname = \"127.0.0.1\"")).
	    		withFallback(ConfigFactory.parseString("akka.cluster.role.workerRole.min-nr-of-members = " +nbWorkers)).
	    		withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://kCoreDescompositionSystem@127.0.0.1:2552\"]").
	    		withFallback(ConfigFactory.load("kcore")));
	    
	    
	    //Creation of an Actor System that corresponds to BLADYG solution
	    final ActorSystem system = ActorSystem.create("kCoreDescompositionSystem", configLocal);
	    
	    system.log().info("Kcore Decomposition will start when at least"+nbWorkers+" workers node join the cluster.");
	    
	    //Register the current actor as a Master worker 

	    //#registerOnUp
	    Cluster.get(system).registerOnMemberUp(new Runnable() {
	      @Override
	      public void run() {
	    			  system.actorOf(Props.create(Master.class, nbWorkers, nbPartitions,graphFile,partFile,partMethod,nbInsertions,nbDeletions,TypeOfEdges),"master");
	      }
	    });
	    //#registerOnUp
	  }
	 else
	 {
		 System.out.println("Check your parameters on Master side");
	 }
	 
  }

  
  public static String getMasterIP(String FileIPs) throws IOException
  {
  	BufferedReader reader = new BufferedReader(new FileReader(FileIPs));
  	String line="";
  	String ip="";
  	while((line=reader.readLine())!=null)
  	{
  		
  		if(!line.equals(""))
  		{
  			String[] parts = line.split("\\s");
  			if(parts[0].toUpperCase().equals("MASTER"))
  				ip= parts[2]; 
  		}
  	}
  	
  	reader.close();
  	return ip;
  }

  public static String getSeedNodesConf(String FileIPs) throws IOException
  {
  	BufferedReader reader = new BufferedReader(new FileReader(FileIPs));
  	String line="";
  	String seed="[";
  	//int port=0;
  	while((line=reader.readLine())!=null)
  	{
  		if(!line.equals(""))
  		{
  			String[] parts = line.split("\\s");
  			seed+="\"akka.tcp://kCoreDescompositionSystem@"+parts[2]+":2552\","; 

  		}
  	}
  	seed = seed.substring(0, seed.length()-1); 
  	seed+="]";
  	reader.close();
  	return seed;
  }

}

