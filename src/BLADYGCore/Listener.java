package BLADYGCore;

import akka.actor.Address;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class Listener extends UntypedActor {
	
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	Cluster cluster = Cluster.get(getContext().system());

	  //subscribe to cluster changes
	  @Override
	  public void preStart() {
	    //#subscribe
	    cluster.subscribe(getSelf(), MemberEvent.class);
	   // Address clusterAddress = Cluster.get(getContext().system()).selfAddress();
	   // Cluster.get(getContext().system()).join(clusterAddress);
	    //#subscribe
	  }

	  //re-subscribe when restart
	  @Override
	  public void postStop() {
	    cluster.unsubscribe(getSelf());
	  }

public void onReceive(Object message) 
{
	if (message instanceof String) 
	{
		System.out.println(String.format("\n\tMessage from the Master: Done"));
		getContext().system().shutdown();
		
		

	} 
	else 
	{
		unhandled(message);
	}
}
}