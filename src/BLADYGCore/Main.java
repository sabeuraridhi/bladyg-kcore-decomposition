package BLADYGCore;

import java.io.IOException;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.actor.UntypedActorFactory;
import akka.cluster.ClusterEvent.ClusterDomainEvent;
import akka.cluster.Cluster;
import akka.routing.ConsistentHashingRouter;


public class Main {
	
	 public static void main(String[] args) throws InterruptedException, IOException {
			
		 	//Starting the workers
		 	//in a real cluster, the jar file that runs WorkerMain class should be executed in every worker machine  
		 	WorkerMain.main(new String []{"1", "ec2machines","8"});
			WorkerMain.main(new String []{"2", "ec2machines","8"});
			WorkerMain.main(new String []{"3", "ec2machines","8"});
			WorkerMain.main(new String []{"4", "ec2machines","8"});	
			WorkerMain.main(new String []{"5", "ec2machines","8"});
			WorkerMain.main(new String []{"6", "ec2machines","8"});
			WorkerMain.main(new String []{"7", "ec2machines","8"});
			WorkerMain.main(new String []{"8", "ec2machines","8"});
			
			/*Starting the master. The jar file that runs MasterMain class should be executed in the master machine 
			The list of parameters of the MasterMain class are : 
			(1) the graph file, 
			(2) the partitioning file, 
			(3) the used partitioning method (in the current version of, only RANDOM and METIS methods are supported),  
			(4) the number of partitions 
			(5) the number of workers 
			(6) the number of frontier edges to be used to evaluate the BLADYG solution for the task of distributed k-core decomposition 
			(7) the type of frontier edges (INTERNAL or EXTERNAL). INTERNAL means inter-block partition edges and EXTERNAL means intra-block partition edges (please see the corresponding paper: Aridhi et al, 2016 @ DEBS 2016)
			(8) the file containing the ip addresses of the master and the slaves (in the case of a real cluster)
			*/
			MasterMain.main(new String []{"data/ds_1000000","data/randomPartitioning_1000000_8","RANDOM","8","8","1000", "EXTERNAL", "ec2machines"});
	 }
}
