package BLADYGCore;

import graphTools.GraphTools;
import graphTools.Statistics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.Random;

import message.CalculateMsg;
import message.CorenessOfNodeMsg;
import message.DistantKCoreResultMsg;
import message.DistantUpdateMsg;
import message.DoNextFrontierEdgeMsg;
import message.FinishUpdateMsg;
import message.GetReachableNodesMsg;
import message.IncrementWorkersMsg;
import message.PropagateReachableNodesW2WAnswer;
import message.RemoteKCoreMsg;
import message.RemoteKCoreUpdateMsg;
import message.SetInfoWorkersMsg;
import message.UpdateNeighborOfOneNodeMsg;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.Props;
import akka.actor.ReceiveTimeout;
import akka.actor.UntypedActor;
import akka.pattern.Patterns;
import akka.routing.FromConfig;
import akka.routing.RoundRobinRouter;
import akka.actor.ActorPath;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.dispatch.*;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;
import scala.concurrent.Await;
import scala.concurrent.Promise;
import structure.Edge;
import structure.IntArray;
import akka.util.Timeout;
import akka.actor.Address;
import akka.actor.AddressFromURIString;
import akka.remote.routing.RemoteRouterConfig;

// This class implement the Master actor. 
public class Master extends UntypedActor {

	
private final int nrOfPartitions;
private final String graphFile;
private final String partFile;
private final String partMethod;
private final int nbInsertions;
private final int nbDeletions;
private final String TypeOfEdges;


long NumberOfExchangedNodeswithCorenessForInsertions=0;
long NumberOfExchangedNodeswithCorenessForDeletions=0;
long NumberOfUpdatedNodesForInsertions=0,MaxNumberOfUpdatedNodesForInsertions=0,MinNumberOfUpdatedNodesForInsertions=0;
long NumberOfUpdatedNodesForDeletions=0,MaxNumberOfUpdatedNodesForDeletions=0,MinNumberOfUpdatedNodesForDeletions=0;
Vector NbExchangedNodesForInsertions=new Vector();
Vector NbExchangedNodesForDeletions=new Vector();
long NumberOfMsg=0;

static int nbCandidateNodesNotifications=0,nbUpdateNotifications=0,nbCandidateNodesNotificationsCase1=0;
private final int nrOfWorkers;

static int runningWorkers=0;
HashSet vectorFusionOfReachableNodes,vectorCurrentReachableNodes;
Hashtable hashCurrentOfNeighborsOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashCurrentCorenessNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes;

static   Hashtable<Integer, ActorRef>  InfoWorkers;
//private   Hashtable<Edge, Integer>  CandNodesHash;

static int indiceEdges=0;
static int nrOfResults=0;
//private final long start = System.currentTimeMillis();
//private ActorRef workerRouter;
long startTime ;
Vector frontierEdges,runtimesInsertions,runtimesDeletions, randomEdges;
Hashtable<Integer,Integer> InvertedHashPartitions;
public static Hashtable<Integer,Integer> HashPartitions;

ActorRef backend = getContext().actorOf(FromConfig.getInstance().props(),"workerRouter");
LoggingAdapter log = Logging.getLogger(getContext().system(), this);
Cluster cluster = Cluster.get(getContext().system());
		
		
//subscribe to cluster changes
@Override
public void preStart() throws IOException, InterruptedException {
//#subscribe
cluster.subscribe(getSelf(), MemberEvent.class);
log.info("Initializing  (Step1)...");
CalculateMsg();

}

//re-subscribe when restart
@Override
public void postStop() {
		   cluster.unsubscribe(getSelf());
}

//Constructor of the Master		  
public Master(final int nrOfWorkers, int nrOfPartitions, String graphFile, String partFile, String partMethod, int nbInsertions, int nbDeletions, String TypeOfEdges) throws IOException 
{
	this.nrOfPartitions = nrOfPartitions;
	this.nrOfWorkers = nrOfWorkers;
	this.graphFile = graphFile;
	this.partFile = partFile;
	this.partMethod = partMethod;
	this.TypeOfEdges = TypeOfEdges;
	
	this.nbInsertions=nbInsertions;
	this.nbDeletions=nbDeletions;
	InfoWorkers=new Hashtable<Integer, ActorRef>();
	int flag=0;
	
	if(partMethod.toUpperCase().equals("METIS"))
	{
		InvertedHashPartitions= GraphTools.constructPartitionsMetis(partFile, graphFile, "frontieredgesfile",nrOfPartitions, "Part_");
		System.out.println("Distribution of the nodes over partitions: "+InvertedHashPartitions);
	}
	else if (partMethod.toUpperCase().equals("RANDOM"))
	{
		InvertedHashPartitions= GraphTools.constructPartitionsRandom(partFile, graphFile, "frontieredgesfile",nrOfPartitions, "Part_");
		System.out.println("Distribution of the nodes over partitions: "+InvertedHashPartitions);
	}
	
	
	if(TypeOfEdges.toUpperCase().equals("INTERNAL"))
	{
		System.out.println("Generate a set of randomly selected internal edges");
		frontierEdges=generateRandomEdges(nbInsertions,nbDeletions);	
	}
	else if(TypeOfEdges.toUpperCase().equals("EXTERNAL"))
	{
		System.out.println("Generate a set of edges choosed randomly from the set of frontier edges from the text file");
		frontierEdges=GraphTools.generateFrontierNodesFromFile("frontieredgesfile",nbInsertions,nbDeletions);	
	}
	
	FileWriter f = new FileWriter(graphFile+ "-insertions-"+TypeOfEdges+"-"+nrOfWorkers);
	for(int i=0;i<nbInsertions;i++){
		Edge e=(Edge)frontierEdges.elementAt(i);
		f.write(e.getNode1()+" "+e.getNode2()+"\n");
	}
	f.close();
	

	runtimesInsertions=new Vector();
	runtimesDeletions=new Vector();
	vectorFusionOfReachableNodes=new HashSet();
	hashFusionNeighborsOfReachableNodes=new Hashtable<Integer,IntArray>();
	hashFusionCorenessNeighborsOfReachableNodes=new Hashtable<Integer,Integer>();
	log.info("The Master constructor is executed ...");
}

//Generate a set of randomly chosen frontier edges 
public Vector generateRandomEdges(int nbInsertions, int nbDeletions)
{
	Vector vRandomEdges=new Vector(); 
	Vector vRandomEdgesDeletions=new Vector(); 

	for (int i=0;i<nbInsertions;i++) 
	{
		int node1 =0; 
		int node2 =0; 
		int part1 =0; 
		int part2 =0; 
		do
		{
			node1=GraphTools.randInt(1,HashPartitions.size()-1);
			node2=GraphTools.randInt(1,HashPartitions.size()-1);
			
			if(HashPartitions.containsKey(node1))
				part1=(Integer)HashPartitions.get(node1);
			
			if(HashPartitions.containsKey(node2))
				part2=(Integer)HashPartitions.get(node2);
			
			
		}
		while((part1==0)||(part2==0)||(part1!=part2)||(node1==node2));
		
		vRandomEdges.addElement(new Edge(node1,node2,part1,part2,1));
		vRandomEdgesDeletions.addElement(new Edge(node1,node2,part1,part2,2));	
		//System.out.println("Nodes "+node1+" et "+node2+" are chosed");
	}
	vRandomEdges.addAll(vRandomEdgesDeletions);
	return vRandomEdges;
}
public void CalculateMsg() throws InterruptedException
{
		log.info("nb Partition : "+	nrOfPartitions);
		//Thread.sleep(5000);
		backend.tell(new String(""), getSelf());

	for (int start = 0; start < nrOfPartitions; start++) 
	{	
		log.info("Launching worker "+(start+1));
		Master.runningWorkers++;	
		backend.tell(new RemoteKCoreMsg(1,start+1,InvertedHashPartitions.get(start+1)), getSelf());
		Thread.sleep(1000);
	}
	InfoWorkers.put(0,getSelf());
}



public void onReceive(Object message) throws Exception {
	
	if (message instanceof IncrementWorkersMsg) 
	{
		IncrementWorkersMsg res = (IncrementWorkersMsg) message;
		Master.runningWorkers+=res.getNbIncrementedWorkers();
		getSender().tell(1, getSelf());
		
	}
	else if (message instanceof CalculateMsg) {
		
		for (int start = 0; start < nrOfPartitions; start++) 
		{				
			backend.tell(new RemoteKCoreMsg(1,start+1), getSelf());
		}
		InfoWorkers.put(0,getSelf());
	} 
	
	else if (message instanceof ReceiveTimeout) {
	      log.info("Timeout");
	      //CalculateMsg();

	    }
	else if (message instanceof GetReachableNodesMsg) {
		GetReachableNodesMsg res = (GetReachableNodesMsg) message;
		nbCandidateNodesNotifications++;

		vectorCurrentReachableNodes=res.getReachableNodes();
		hashCurrentOfNeighborsOfReachableNodes=res.getNeighborsOfReachableNodes();
		hashCurrentCorenessNeighborsOfReachableNodes=res.getCorenessOfNeighborsOfReachableNodes();
		
		//update of the vector containing the set of reachable nodes 
		
		vectorFusionOfReachableNodes.addAll(vectorCurrentReachableNodes);

		hashFusionNeighborsOfReachableNodes.putAll(hashCurrentOfNeighborsOfReachableNodes);	
		
		hashFusionCorenessNeighborsOfReachableNodes.putAll(hashCurrentCorenessNeighborsOfReachableNodes);

		
		if (nbCandidateNodesNotifications==Master.runningWorkers)
		{
			Master.runningWorkers=0;
			
			HashSet toBeUpdated;
			Edge e=(Edge)frontierEdges.elementAt(indiceEdges-1);
			
			//Edge deletion
			if(e.getType()==2)
			{
				NumberOfExchangedNodeswithCorenessForDeletions+=hashFusionCorenessNeighborsOfReachableNodes.size();
				NbExchangedNodesForDeletions.addElement(hashFusionCorenessNeighborsOfReachableNodes.size());
				
				
				//Computing the correct values of coreness 
				HashSet vCandidate= new HashSet(vectorFusionOfReachableNodes);
				toBeUpdated=pruneCandidateNodesAkkaForDeletion(vectorFusionOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes);

				vCandidate.removeAll(toBeUpdated);
				
				
				//Constructing the hashtable containing the correct values of coreness for each eligible node
				Hashtable toUpdateDistantNodes= new  Hashtable<Integer, Integer>  ();
				
				Iterator it= vCandidate.iterator();
				while (it.hasNext())
				{
					int elm = (int)it.next();
					toUpdateDistantNodes.put(elm, hashFusionCorenessNeighborsOfReachableNodes.get(elm));

				}
								
				//Ask the workers to do updates
				for(int i=1;i<=nrOfPartitions;i++)
				{
					Master.runningWorkers++;
					ActorRef a=InfoWorkers.get(i);	
					a.tell(new DistantUpdateMsg(toUpdateDistantNodes,getSelf(),i,2), getSelf());
				}
				NumberOfUpdatedNodesForDeletions+=toUpdateDistantNodes.size();
				if(toUpdateDistantNodes.size()>MaxNumberOfUpdatedNodesForDeletions)
				{
					MaxNumberOfUpdatedNodesForDeletions=toUpdateDistantNodes.size();
				}
				if(toUpdateDistantNodes.size()<MinNumberOfUpdatedNodesForDeletions)
				{
					MinNumberOfUpdatedNodesForDeletions=toUpdateDistantNodes.size();
				}
				
			}
			
			//Edge insertion
			else
			{
				NumberOfExchangedNodeswithCorenessForInsertions+=hashFusionCorenessNeighborsOfReachableNodes.size();
				NbExchangedNodesForInsertions.addElement(hashFusionCorenessNeighborsOfReachableNodes.size());
				
				//Computing the correct values of coreness 
				toBeUpdated=pruneCandidateNodesAkka(vectorFusionOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes);
			
				//Constructing the hashtable containing the correct values of coreness for each eligible node
				Hashtable toUpdateDistantNodes= new  Hashtable<Integer, Integer>  ();
					
				Iterator it= toBeUpdated.iterator();
				while (it.hasNext())
				{
					int elm = (int)it.next();
					toUpdateDistantNodes.put(elm, hashFusionCorenessNeighborsOfReachableNodes.get(elm));
				}
				
				//Ask the workers to do updates
				for(int i=1;i<=nrOfPartitions;i++)
				{
					Master.runningWorkers++;
					//ActorRef a=this.getContext().actorFor(InfoWorkers.get(i).toString());
					ActorRef a=InfoWorkers.get(i);

					a.tell(new DistantUpdateMsg(toUpdateDistantNodes,getSelf(),i,1), getSelf());
				}
				NumberOfUpdatedNodesForInsertions+=toUpdateDistantNodes.size();
				if(toUpdateDistantNodes.size()>MaxNumberOfUpdatedNodesForInsertions)
				{
					MaxNumberOfUpdatedNodesForInsertions=toUpdateDistantNodes.size();
				}
				if(toUpdateDistantNodes.size()<MinNumberOfUpdatedNodesForInsertions)
				{
					MinNumberOfUpdatedNodesForInsertions=toUpdateDistantNodes.size();
				}

			}
			
			nbCandidateNodesNotifications=0;
		}
	}
	else if (message instanceof FinishUpdateMsg)
	{
	
			nbUpdateNotifications++;
			
			//Here we check if we got notifications from all workers
			if(nbUpdateNotifications==(Master.InfoWorkers.size()-1))
			{
				Master.runningWorkers=0;	
				getSelf().tell(new DoNextFrontierEdgeMsg(), getSelf());
				nbUpdateNotifications=0;
			}
	}
	else if (message instanceof PropagateReachableNodesW2WAnswer)
	{
		PropagateReachableNodesW2WAnswer result = (PropagateReachableNodesW2WAnswer) message;
		//Master.runningWorkers=0;
		nbCandidateNodesNotificationsCase1++;
		
		//Here we get the set of frontier edges from the different workers
		vectorCurrentReachableNodes=result.getReachableNodes();
		hashCurrentOfNeighborsOfReachableNodes=result.getNeighborsOfReachableNodes();
		hashCurrentCorenessNeighborsOfReachableNodes=result.getCorenessOfNeighborsOfReachableNodes();
		
		vectorFusionOfReachableNodes.addAll(vectorCurrentReachableNodes);
		
		hashFusionNeighborsOfReachableNodes.putAll(hashCurrentOfNeighborsOfReachableNodes);
		
		hashFusionCorenessNeighborsOfReachableNodes.putAll(hashCurrentCorenessNeighborsOfReachableNodes);
		
		if (nbCandidateNodesNotificationsCase1==Master.runningWorkers)
		{	
			nbCandidateNodesNotificationsCase1=0;
			Master.runningWorkers=0; 	
			HashSet toBeUpdated;
			Edge e=(Edge)frontierEdges.elementAt(indiceEdges-1);
			
			//Edge deletion
			if(e.getType()==2)
			{
				
				NumberOfExchangedNodeswithCorenessForDeletions+=hashFusionCorenessNeighborsOfReachableNodes.size();
				NbExchangedNodesForDeletions.addElement(hashFusionCorenessNeighborsOfReachableNodes.size());
				
				//Computing the correct values of coreness 
				HashSet vCandidate= new HashSet(vectorFusionOfReachableNodes);

				toBeUpdated=pruneCandidateNodesAkkaForDeletion(vectorFusionOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes);				
				
				vCandidate.removeAll(toBeUpdated);
				
				
				//Constructing the hashtable containing the correct values of coreness for each eligible node
				Hashtable toUpdateDistantNodes= new  Hashtable<Integer, Integer>  ();
				
				Iterator it= vCandidate.iterator();
				while (it.hasNext())
				{
					int elm = (int)it.next();
					toUpdateDistantNodes.put(elm, hashFusionCorenessNeighborsOfReachableNodes.get(elm));

				}
				
				//Ask the workers to apply the updates
				for(int i=1;i<=nrOfPartitions;i++)
				{
					Master.runningWorkers++;
					ActorRef a=InfoWorkers.get(i);
					a.tell(new DistantUpdateMsg(toUpdateDistantNodes,getSelf(),i,2), getSelf());
				}
				NumberOfUpdatedNodesForDeletions+=toUpdateDistantNodes.size();
				if(toUpdateDistantNodes.size()>MaxNumberOfUpdatedNodesForDeletions)
				{
					MaxNumberOfUpdatedNodesForDeletions=toUpdateDistantNodes.size();
				}
				if(toUpdateDistantNodes.size()<MinNumberOfUpdatedNodesForDeletions)
				{
					MinNumberOfUpdatedNodesForDeletions=toUpdateDistantNodes.size();
				}
				
			}
			
			//Edge insertion
			else
			{
				
				NumberOfExchangedNodeswithCorenessForInsertions+=hashFusionCorenessNeighborsOfReachableNodes.size();
				NbExchangedNodesForInsertions.addElement(hashFusionCorenessNeighborsOfReachableNodes.size());
				
				//Computing the correct values of coreness 
				toBeUpdated=pruneCandidateNodesAkka(vectorFusionOfReachableNodes,hashFusionNeighborsOfReachableNodes,hashFusionCorenessNeighborsOfReachableNodes);
			
				//Constructing the hashtable containing the correct values of coreness for each eligible node
				Hashtable toUpdateDistantNodes= new  Hashtable<Integer, Integer>  ();
				
				Iterator it= toBeUpdated.iterator();
				while (it.hasNext())
				{
					int elm = (int)it.next();
					toUpdateDistantNodes.put(elm, hashFusionCorenessNeighborsOfReachableNodes.get(elm));
				}
				
				//Ask the workers to do updates
				for(int i=1;i<=nrOfPartitions;i++)
				{
					Master.runningWorkers++;
					ActorRef a=InfoWorkers.get(i);
					a.tell(new DistantUpdateMsg(toUpdateDistantNodes,getSelf(),i,1), getSelf());
					
				}
				
				NumberOfUpdatedNodesForInsertions+=toUpdateDistantNodes.size();
				if(toUpdateDistantNodes.size()>MaxNumberOfUpdatedNodesForInsertions)
				{
					MaxNumberOfUpdatedNodesForInsertions=toUpdateDistantNodes.size();
				}
				if(toUpdateDistantNodes.size()<MinNumberOfUpdatedNodesForInsertions)
				{
					MinNumberOfUpdatedNodesForInsertions=toUpdateDistantNodes.size();
				}

			}
		}

	}
	else if (message instanceof DoNextFrontierEdgeMsg)
	{
		vectorFusionOfReachableNodes.clear();
		hashFusionNeighborsOfReachableNodes.clear();
		hashFusionCorenessNeighborsOfReachableNodes.clear();
		
		Master.runningWorkers=0;
		
		if(indiceEdges!=0)
		{
			long endTime   = System.currentTimeMillis();
			
			long totalTime = endTime - startTime;
			
			System.out.println("Time:"+totalTime);
			Edge e=(Edge)frontierEdges.elementAt(indiceEdges-1);
			if(e.getType()==1)
				runtimesInsertions.addElement(totalTime);
			if(e.getType()==2)
				runtimesDeletions.addElement(totalTime);
		}
		
		
		if((runtimesDeletions.size()==nbDeletions)&&(runtimesInsertions.size()==nbInsertions))
		{
			long total=0, totalD=0;
			
			for(int i=0;i<nbInsertions;i++)
			{
				total+=(long)runtimesInsertions.elementAt(i);
			}
			totalD=0;
			for(int i=0;i<nbDeletions;i++)
			{
				totalD+=(long)runtimesDeletions.elementAt(i);
			}
			
			
			double avgNumberExchangedInsertions=(double)(NumberOfExchangedNodeswithCorenessForInsertions)/nbInsertions;
			double avgNumberUpdatedInsertions=(double)(NumberOfUpdatedNodesForInsertions)/nbInsertions;
			
			double avgNumberExchangedDeletions=(double)(NumberOfExchangedNodeswithCorenessForDeletions)/nbDeletions;
			double avgNumberUpdatedDeletions=(double)(NumberOfUpdatedNodesForDeletions)/nbDeletions;
			
			System.out.println("[Insertion] AVG Insertion Time for "+nbInsertions+" insertions is: "+(total/nbInsertions)+" ms");
			System.out.println("[Deletion] AVG Deletion Time for "+nbDeletions+" deletions is: "+(totalD/nbDeletions)+" ms");
			
			System.out.println("[Insertion] AVG Number of exchanged nodes with coreness values = "+NumberOfExchangedNodeswithCorenessForInsertions+" / "+nbInsertions+" = "+avgNumberExchangedInsertions);
			System.out.println("[Deletion] AVG Number of exchanged nodes with coreness values = "+NumberOfExchangedNodeswithCorenessForDeletions+" / "+nbDeletions+" = "+avgNumberExchangedDeletions);
			
			System.out.println("[Insertion] Max value of Number of updated nodes = "+MaxNumberOfUpdatedNodesForInsertions);
			System.out.println("[Deletion] Max value of Number of updated nodes = "+MaxNumberOfUpdatedNodesForDeletions);
			
			System.out.println("[Insertion] Min value of Number of updated nodes = "+MinNumberOfUpdatedNodesForInsertions);
			System.out.println("[Deletion] Min value of Number of updated nodes = "+MinNumberOfUpdatedNodesForDeletions);
			
			System.out.println("[Insertion] AVG Number of updated nodes = "+NumberOfUpdatedNodesForInsertions+" / "+nbInsertions+" = "+avgNumberUpdatedInsertions);
			System.out.println("[Deletion] AVG Number of updated nodes = "+NumberOfUpdatedNodesForDeletions+" / "+nbDeletions+" = "+avgNumberUpdatedDeletions);
			
			
			System.out.println("[Insertion] Mean value= "+Statistics.mean(NbExchangedNodesForInsertions));
			System.out.println("[Deletion] Mean value= "+Statistics.mean(NbExchangedNodesForDeletions));
			
			System.out.println("[Insertion] Standard Deviation= "+Statistics.stdDev(NbExchangedNodesForDeletions));
			System.out.println("[Deletion] Standard Deviation= "+Statistics.stdDev(NbExchangedNodesForInsertions));
			System.exit(0);
		}		
		
			if(frontierEdges.size()!=indiceEdges)
			{
			    
				
				Edge e=(Edge)frontierEdges.elementAt(indiceEdges);
				if(e.getType()==1)
				System.out.println("Adding an edge numer "+(indiceEdges+1)+" ("+ e.getNode1()+","+e.getNode2()+")");
				if(e.getType()==2)
					System.out.println("Deleting an edge numer "+(indiceEdges+1)+" ("+ e.getNode1()+","+e.getNode2()+")");
								
				indiceEdges++;				
				//Retrieving coreness values of frontier nodes			
			    Timeout timeout = new Timeout(Duration.create(20, "seconds"));
			    
			    ActorRef aa1=InfoWorkers.get(e.getPartition1());
			    			    
			    ActorRef aa2=InfoWorkers.get(e.getPartition2());
				long masterStartTime   = System.currentTimeMillis();

			    Future<Object> future1 = Patterns.ask(aa1, new CorenessOfNodeMsg(e.getNode1(),getSelf()), timeout);
			    Future<Object> future2 = Patterns.ask(aa2, new CorenessOfNodeMsg(e.getNode2(),getSelf()), timeout);
			    
			    Integer result1 = (int) Await.result(future1, timeout.duration());
			    Integer result2 = (int) Await.result(future2, timeout.duration());
			    
			    e.setCoreness1(result1.intValue());
			    e.setCoreness2(result2.intValue());				
				if(e.getPartition1()!=e.getPartition2())
				{
				    startTime = System.currentTimeMillis();

					if (e.getCoreness1()<e.getCoreness2())
					{
						System.out.println("k("+e.getNode1()+")["+e.getCoreness1()+"] < k("+e.getNode2()+")["+e.getCoreness2()+"]");
						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition1());
						ActorRef a2=InfoWorkers.get(e.getPartition2());
					
						RemoteKCoreUpdateMsg wm1=new RemoteKCoreUpdateMsg(2,e,e.getNode1());
						wm1.setCurrentPartition(e.getPartition1());
						a1.tell(wm1, getSelf());
						a2.tell(new UpdateNeighborOfOneNodeMsg(e.getNode2(),e.getNode1(),e.getCoreness1(),e.getPartition1(),e.getType()), getSelf());
					}
					else if (e.getCoreness1()>e.getCoreness2())
					{
						System.out.println("k("+e.getNode1()+")["+e.getCoreness1()+"] > k("+e.getNode2()+")["+e.getCoreness2()+"]");

						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition2());
						ActorRef a2=InfoWorkers.get(e.getPartition1());
	
						RemoteKCoreUpdateMsg wm2=new RemoteKCoreUpdateMsg(2,e,e.getNode2());
						wm2.setCurrentPartition(e.getPartition2());
	
						a1.tell(wm2, getSelf());
						a2.tell(new UpdateNeighborOfOneNodeMsg(e.getNode1(),e.getNode2(),e.getCoreness2(),e.getPartition2(),e.getType()), getSelf());
					}
					else 
					{	
						System.out.println("k("+e.getNode1()+")["+e.getCoreness1()+"] = k("+e.getNode2()+")["+e.getCoreness2()+"]");

						Master.runningWorkers++;
						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition1()); 
						a1.tell(new GetReachableNodesMsg(e,e.getNode1(),e.getPartition1()), getSelf()); 
						ActorRef a2=InfoWorkers.get(e.getPartition2()); 
						a2.tell(new GetReachableNodesMsg(e,e.getNode2(),e.getPartition2()), getSelf());				
					}
				}
				else
				{
				    startTime = System.currentTimeMillis();
	
					if (e.getCoreness1()<e.getCoreness2())
					{
						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition1());
						RemoteKCoreUpdateMsg wm1=new RemoteKCoreUpdateMsg(2,e,e.getNode1());
						wm1.setCurrentPartition(e.getPartition1());
						a1.tell(wm1, getSelf());
					}
					else if (e.getCoreness1()>e.getCoreness2())
					{
						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition2());
						RemoteKCoreUpdateMsg wm2=new RemoteKCoreUpdateMsg(2,e,e.getNode2());
						wm2.setCurrentPartition(e.getPartition2());
						a1.tell(wm2, getSelf());
					}
					else 
					{	
						Master.runningWorkers++;
						Master.runningWorkers++;
						ActorRef a1=InfoWorkers.get(e.getPartition1()); 
						a1.tell(new RemoteKCoreUpdateMsg(2,e,e.getNode1()), getSelf()); 
				
						ActorRef a2=InfoWorkers.get(e.getPartition2());
						a2.tell(new RemoteKCoreUpdateMsg(2,e,e.getNode2()), getSelf());				
					}
				}			
			}
			else
			{
				long total=0;
				for(int i=0;i<frontierEdges.size();i++)
				{
					total+=(long)runtimesInsertions.elementAt(i);
				}
				
				
				System.out.println("No frontier edges left");
				showWorkers();
				double avgNumberExchanged=(double)(NumberOfExchangedNodeswithCorenessForInsertions)/frontierEdges.size();
				double avgNumberUpdated=(double)(NumberOfUpdatedNodesForInsertions)/frontierEdges.size();
				System.out.println("[Insertion] AVG Insertion Time for "+frontierEdges.size()+" insertions is: "+(total/frontierEdges.size())+" ms");
				System.out.println("[Insertion] AVG Number of exchanged nodes with coreness values = "+NumberOfExchangedNodeswithCorenessForInsertions+" / "+frontierEdges.size()+" = "+avgNumberExchanged);
				System.out.println("[Insertion] Max value of Number of updated nodes = "+MaxNumberOfUpdatedNodesForInsertions);
				System.out.println("[Insertion] Min value of Number of updated nodes = "+MinNumberOfUpdatedNodesForInsertions);
				System.out.println("[Insertion] AVG Number of updated nodes = "+NumberOfUpdatedNodesForInsertions+" / "+frontierEdges.size()+" = "+avgNumberUpdated);
				System.out.println("[Insertion] Mean value= "+Statistics.mean(NbExchangedNodesForInsertions));
				System.out.println("[Insertion] Standard Deviation= "+Statistics.stdDev(NbExchangedNodesForInsertions));
				
				//shut down the BLADYG system
				getContext().system().shutdown();
			}
	}
	else if (message instanceof DistantKCoreResultMsg)
	{
		DistantKCoreResultMsg result = (DistantKCoreResultMsg) message;
		InfoWorkers.put(result.getPartition(),result.getPath());
		
		nrOfResults ++;
		if (nrOfResults == nrOfPartitions) 
		{
			Master.runningWorkers=0;
			nrOfResults=0;
			Timeout timeout = new Timeout(Duration.create(20, "seconds"));
			backend.tell(new String(""), getSelf());
			for (int start = 1; start <= nrOfPartitions; start++) 
			{					
				log.info("Sending to Worker: "+InfoWorkers.get(start));
				Future<Object> future1 = Patterns.ask(backend, new SetInfoWorkersMsg(InfoWorkers, start), timeout);
				 Integer result1 = (int) Await.result(future1, timeout.duration());
				//Thread.sleep(1000);
			}

			getSelf().tell(new DoNextFrontierEdgeMsg(), getSelf());
		}
		
	} 
	else 
	{
		unhandled(message);
	}
}

//Show the master and the list of workers
public void showWorkers()
{
	
	Enumeration hkeys=InfoWorkers.keys();
	
	while(hkeys.hasMoreElements())
	{
		int key=(int) hkeys.nextElement();
		if(key==0)
		{
			System.out.println("The Master node: "+InfoWorkers.get(key));
		}
		else
		{
			System.out.println("Partition: "+key+" is processed by Worker "+InfoWorkers.get(key));
		}
		
	}
}
public HashSet pruneCandidateNodesAkka(HashSet cand, Hashtable h, Hashtable hc)
{
	int flag=0;
	int count=0;	
	HashSet cand2=new HashSet(cand);
	if(cand ==null) {return null;}
	Iterator it= cand2.iterator();
	
	while (it.hasNext())
	{
		
		count=0;
		int elm = (int) it.next();
		
		IntArray ng = (IntArray) h.get(elm);

		for(int j=0;j<ng.size();j++)
		{
			if(cand.contains(ng.get(j)))
			{
				count++;				
			}			
			else if ((int)hc.get(ng.get(j))>(int)hc.get(elm))
			{
				count++;	
			}
		}			
		if(count<=(int)hc.get(elm))
		{			
			cand.remove(elm);
			flag=1;
		}	
	}
	
	
	if(flag==1)
	{
		pruneCandidateNodesAkka(cand,h,hc);
	}
	return cand;
}
public HashSet pruneCandidateNodesAkkaForDeletion(HashSet cand, Hashtable h, Hashtable hc)
{
	int flag=0;		
	int count=0;	
	HashSet cand2 = new HashSet(cand);
	if(cand ==null) {return null;}
	Iterator it= cand2.iterator();
	while (it.hasNext()) {
				count=0;
				int elm= (int) it.next();
				IntArray ng = (IntArray) h.get(elm);

				for(int j=0;j<ng.size();j++)
				{
					if(cand.contains(ng.get(j)))
					{
						count++;				
					}			
					else if ((int)hc.get(ng.get(j))>(int)hc.get(elm))
					{
						count++;	
					}
				}	
				if(count<(int)hc.get(elm))
				{
					cand.remove(elm);
					flag=1;
				}
	}
	
	if(flag==1)
	{
		pruneCandidateNodesAkkaForDeletion(cand,h,hc);
	}
	return cand;
}
}

 
