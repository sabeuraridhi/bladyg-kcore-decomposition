package BLADYGCore;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import message.CorenessOfNodeMsg;
import message.DistantKCoreResultMsg;
import message.DistantUpdateMsg;
import message.FinishUpdateMsg;
import message.GetReachableNodesMsg;
import message.IncrementWorkersMsg;
import message.PropagateReachableNodesW2WAnswer;
import message.PropagateReachableNodesW2WCase1Msg;
import message.PropagateReachableNodesW2WCase2Msg;
import message.RemoteKCoreMsg;
import message.RemoteKCoreUpdateMsg;
import message.SetInfoWorkersMsg;
import message.UpdateNeighborOfOneNodeMsg;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import structure.DistKCore;
import structure.Edge;
import structure.IntArray;
import structure.IntGraph;
import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.UntypedActor;
import akka.pattern.Patterns;
import akka.util.Timeout;
import akka.cluster.Member;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.actor.RootActorPath;
import akka.actor.Actor;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.MemberStatus;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.cluster.ClusterEvent.MemberRemoved;
import akka.cluster.ClusterEvent.UnreachableMember;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import message.*;

public class Worker extends UntypedActor {
	 LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	 Timeout timeout = new Timeout(Duration.create(20, "seconds"));
	 Cluster cluster = Cluster.get(getContext().system());
	 static   Hashtable<Integer, ActorRef>  InfoWorkersInWorker;
	 
	
	DistKCore d;
	IntGraph part;
	    
	static Vector propagatedNodes=new Vector();
	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof BatchPropagationCase1Msg) 
		{
			BatchPropagationCase1Msg work = (BatchPropagationCase1Msg) message;
		    ArrayList<PropagateReachableNodesW2WCase1Msg> listOfMessage = work.getListOfMessage1();    
		    System.out.println("[case1] list of messages contains: "+listOfMessage.size());   
		}
		else if (message instanceof BatchPropagationCase2Msg) 
		{	
			BatchPropagationCase2Msg work = (BatchPropagationCase2Msg) message;
		    ArrayList<PropagateReachableNodesW2WCase2Msg> listOfMessage = work.getListOfMessage2();
		    System.out.println("[case2] [worker "+this.getSelf()+"]list of messages contains: "+listOfMessage.size());    
		}
		else if (message instanceof String) 
		{
		}
		else if (message instanceof SetInfoWorkersMsg) 
		{
			SetInfoWorkersMsg work = (SetInfoWorkersMsg) message;
			InfoWorkersInWorker=work.getInfoWorkersInWorker();
			System.out.println("Initilaizing variables on Worker "+work.getPartition());
			getSender().tell(1, getSelf());
		}
		else if (message instanceof CorenessOfNodeMsg) 
		{
			CorenessOfNodeMsg work = (CorenessOfNodeMsg) message;
			if(!part.distNeighbCoreness.containsKey(work.getNode()))
			{
				
				part.distNeighbCoreness.put(work.getNode(), 0);
			}
			getSender().tell(new Integer(part.distNeighbCoreness.get(work.getNode())), getSelf());
			
		}
		else if (message instanceof PropagateReachableNodesW2WCase1Msg) 
		{
			
			
			PropagateReachableNodesW2WCase1Msg work = (PropagateReachableNodesW2WCase1Msg) message;
			Worker.propagatedNodes.addElement(work.getNode());
			part.reachableNodes.clear();
			Hashtable h= new Hashtable<Integer,IntArray>();
			Hashtable hc= new Hashtable<Integer,Integer>();

			
				part.inducedCoreSubgraphAkka(work.getNode());	
				
			    HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase1Msg>> listOfMessage1 = new HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase1Msg>>();

				int countworkers=0; 
				initListOfMessage1(listOfMessage1);

				Iterator it = part.reachableNodes.iterator();
				while(it.hasNext())
				{
					int elm =(int) it.next();
					if(!part.frontierNodes.contains(elm))
					{
						hc.put(elm, part.distNeighbCoreness.get(elm));
						IntArray ia=part.neighborsAkka(elm);
							h.put(elm, ia);
							for(int i=0;i<ia.size();i++)
							{
								if(part.distNeighbCoreness.containsKey(ia.get(i)))
								{
										hc.put(ia.get(i), part.distNeighbCoreness.get(ia.get(i)));
								}
							}
					}
					
					else if (!Worker.propagatedNodes.contains(elm))
					{
						Worker.propagatedNodes.addElement(elm);
						countworkers++;				
						PropagateReachableNodesW2WCase1Msg workMsg= new PropagateReachableNodesW2WCase1Msg(elm);						
						listOfMessage1.get(part.distNeighbPartition.get(elm)).add(workMsg);
					}
				}
				
				ActorRef master=InfoWorkersInWorker.get(0);
				if(countworkers>0)
				{
					Future<Object> future1 = Patterns.ask(master, new IncrementWorkersMsg(countworkers), timeout);
					Integer result1 = (int) Await.result(future1, timeout.duration());
					ActorRef a=null;
					for (int i=1;i<=InfoWorkersInWorker.size();i++)
					{
							for (int j=0;j<listOfMessage1.get(i).size();j++)
							{
								PropagateReachableNodesW2WCase1Msg prop = listOfMessage1.get(i).get(j);		
								a=InfoWorkersInWorker.get(part.distNeighbPartition.get(prop.getNode()));
								a.tell(prop, getSelf());
							}
					}
				}
				
				HashSet tmpReachableNodes= new HashSet(part.reachableNodes); 
				PropagateReachableNodesW2WAnswer answer=new PropagateReachableNodesW2WAnswer(tmpReachableNodes,h,hc);
				master.tell(answer, getSelf());
		}
		else if (message instanceof PropagateReachableNodesW2WCase2Msg) 
		{
		
			PropagateReachableNodesW2WCase2Msg work = (PropagateReachableNodesW2WCase2Msg) message;
			

			Hashtable h= new Hashtable<Integer,IntArray>();
			Hashtable hc= new Hashtable<Integer,Integer>();
			Worker.propagatedNodes.addElement(work.getNode());
			part.reachableNodes.clear();
			part.inducedCoreSubgraphAkka(work.getNode());
			
		    HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase2Msg>> listOfMessage2 = new HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase2Msg>>();

			int countworkers=0; 
			initListOfMessage2(listOfMessage2);

			
			Iterator it = part.reachableNodes.iterator();
			while(it.hasNext())
			{
				int elm =(int) it.next();

				if(!part.frontierNodes.contains(elm))
				{
					hc.put(elm, part.distNeighbCoreness.get(elm));
					IntArray ia=part.neighborsAkka(elm);
						h.put(elm, ia);
						for(int i=0;i<ia.size();i++)
						{
							if(part.distNeighbCoreness.containsKey(ia.get(i)))
							{
									hc.put(ia.get(i), part.distNeighbCoreness.get(ia.get(i)));
							}
						}
				}
				
				else if (!Worker.propagatedNodes.contains(elm))
				{
					Worker.propagatedNodes.addElement(elm);	
					countworkers++;
					PropagateReachableNodesW2WCase2Msg workMsg= new PropagateReachableNodesW2WCase2Msg(elm);
					listOfMessage2.get(part.distNeighbPartition.get(elm)).add(workMsg);
				}
			}
			
			ActorRef master=InfoWorkersInWorker.get(0);
			if(countworkers>0)
			{
				Future<Object> future1 = Patterns.ask(master, new IncrementWorkersMsg(countworkers), timeout);
				Integer result1 = (int) Await.result(future1, timeout.duration());
				ActorRef a=null;
				for (int i=1;i<=InfoWorkersInWorker.size();i++)
				{
						for (int j=0;j<listOfMessage2.get(i).size();j++)
						{
							PropagateReachableNodesW2WCase2Msg prop = listOfMessage2.get(i).get(j);
							
							a=InfoWorkersInWorker.get(part.distNeighbPartition.get(prop.getNode()));
	
							a.tell(prop, getSelf());
						}
				}
			}
			
			HashSet tmpReachableNodes= new HashSet(part.reachableNodes); 
			GetReachableNodesMsg answer=new GetReachableNodesMsg(tmpReachableNodes,h,hc);
			master.tell(answer, getSelf());
		}
		else if (message instanceof UpdateNeighborOfOneNodeMsg) 
		{
			UpdateNeighborOfOneNodeMsg work = (UpdateNeighborOfOneNodeMsg) message;
			if(work.getTypeEdge()==1)
			{
				part.addEdge(work.getNode(), work.getNeighbor());
				
				if(!part.frontierNodes.contains(work.getNeighbor()))
				{
					part.frontierNodes.addElement(work.getNeighbor());
					part.distNeighbPartition.put(work.getNeighbor(), work.getPartitionNeighb());
				}
			}
		}
		
		//Step 1: Computing the coreness of nodes in the distant partition
		if (message instanceof RemoteKCoreMsg) 
		{
			RemoteKCoreMsg work = (RemoteKCoreMsg) message;
			System.out.println("\n\tThe work to do by the worker "+this.getSelf()+" is: Step "+work.getStep()+" / Partition: "+work.getPartition());
				d = new DistKCore();
				long  startTime = System.currentTimeMillis();
				doComputation(work.getStep(),work.getPartition(),work.getNbNodes());
				long  endTime = System.currentTimeMillis();
				long totalTime = endTime - startTime;
				System.out.println("Construction time: "+totalTime+" ms");
				getSender().tell(new DistantKCoreResultMsg(work.getPartition(), this.getSelf()), getSelf());
				part.intialize();
		}
		
		//Step 2 - Case 1/Case 2 [ k(u) < k(v)  or k(v) < k(u) ] 
		else if (message instanceof RemoteKCoreUpdateMsg) 
		{
			RemoteKCoreUpdateMsg work = (RemoteKCoreUpdateMsg) message;
			Edge e= work.getEdge();
			if(e.getPartition2()==e.getPartition1())
			{	
				if(e.getType()==1)
				{
					part.addEdgeAkka(e.getNode1(), e.getNode2(), e.getPartition1(), e.getPartition2(), e.getCoreness1(), e.getCoreness2());
					part.addEdgeAkka(e.getNode2(), e.getNode1(), e.getPartition2(), e.getPartition1(), e.getCoreness2(), e.getCoreness1());
					part.distNeighbCoreness.put(e.getNode1(), e.getCoreness1());
					part.distNeighbCoreness.put(e.getNode2(), e.getCoreness2());
				}
				else
				{
					part.removeEdge(e.getNode1(), e.getNode2());
				}
			}
			else if(e.getNode1()==work.getDefaultNode())
			{
				if(e.getType()==1)
				{
					part.addEdgeAkka(e.getNode1(), e.getNode2(), e.getPartition1(), e.getPartition2(), e.getCoreness1(), e.getCoreness2());
					//update the coreness of the distant node
					part.distNeighbCoreness.put(e.getNode2(), e.getCoreness2() );
					//update the partition identifier of the distant node
					part.distNeighbPartition.put(e.getNode2(), e.getPartition2());
					
					if(!part.frontierNodes.contains(e.getNode2()))
						part.frontierNodes.addElement(e.getNode2());
					}
				else
				{
					part.removeEdge(e.getNode1(), e.getNode2());
				}
			}
			else
			{
				if(e.getType()==1)
				{
					part.addEdgeAkka(e.getNode2(), e.getNode1(), e.getPartition2(), e.getPartition1(), e.getCoreness2(), e.getCoreness1());
					//update the coreness of the distant node
					part.distNeighbCoreness.put(e.getNode1(), e.getCoreness1() );	
					//update the parition identifier of the distant node
					part.distNeighbPartition.put(e.getNode1(), e.getPartition1());
					
					if(!part.frontierNodes.contains(e.getNode1()))
						part.frontierNodes.addElement(e.getNode1());
				
				}
				else
				{
					part.removeEdge(e.getNode1(), e.getNode2());
				}
			}
			
			
			//Add the current node to list of propagated nodes
			Worker.propagatedNodes.addElement(work.getDefaultNode());
			
			part.inducedCoreSubgraphAkka(work.getDefaultNode());
			
			//Constructing the set of neighbors of reachable nodes
			//Constructing the set of coreness of neighbors of reachable nodes
			Hashtable h= new Hashtable<Integer,IntArray>();
			Hashtable hc= new Hashtable<Integer,Integer>();	
		    HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase1Msg>> listOfMessage1 = new HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase1Msg>>();
			initListOfMessage1(listOfMessage1);
			int countworkers=0; 
			
			
			Iterator it = part.reachableNodes.iterator();
			while(it.hasNext())
			{
				int elm =(int) it.next();
				if(!part.frontierNodes.contains(elm))
				{
					hc.put(elm, part.distNeighbCoreness.get(elm));

					IntArray ia=part.neighborsAkka(elm);
						h.put(elm, ia);
						
						for(int i=0;i<ia.size();i++)
						{
							if(part.distNeighbCoreness.containsKey(ia.get(i)))
							{
									hc.put(ia.get(i), part.distNeighbCoreness.get(ia.get(i)));
							}
							else
							{
								System.out.println("No coreness of node:"+ia.get(i));
							}
						}
				}
				
				else if (!Worker.propagatedNodes.contains(elm))
				{
					Worker.propagatedNodes.addElement(elm);
					countworkers++;
					
					PropagateReachableNodesW2WCase1Msg workMsg= new PropagateReachableNodesW2WCase1Msg(elm);					
					listOfMessage1.get(part.distNeighbPartition.get(elm)).add(workMsg);					
				}
			}
			
			
			if(countworkers>0)
			{
				ActorRef master=InfoWorkersInWorker.get(0);
				Future<Object> future1 = Patterns.ask(master, new IncrementWorkersMsg(countworkers), timeout);
				Integer result1 = (int) Await.result(future1, timeout.duration());
				//System.out.println("Increment the number of workers by: "+countworkers);
	
				ActorRef a=null;
				for (int i=1;i<=InfoWorkersInWorker.size();i++)
				{
						for (int j=0;j<listOfMessage1.get(i).size();j++)
						{
							PropagateReachableNodesW2WCase1Msg prop = listOfMessage1.get(i).get(j);
							
							
							a=InfoWorkersInWorker.get(part.distNeighbPartition.get(prop.getNode()));
	
							a.tell(prop, getSelf());
						}
				}
			}
			HashSet tmpReachableNodes= new HashSet(part.reachableNodes); 

			PropagateReachableNodesW2WAnswer answer=new PropagateReachableNodesW2WAnswer(tmpReachableNodes,h,hc);
			
			getSender().tell(answer, getSelf());
			part.reachableNodes.clear();
		}
		
		//Step 2 - Case 3 [k(u) = k(v)] - Look for Candidate Nodes in the current partition
		else if (message instanceof GetReachableNodesMsg) 
		{	
			GetReachableNodesMsg r = (GetReachableNodesMsg) message; 		
			int distantNode=r.getEdge().getNode1();
			int localNode= r.getEdge().getNode2();
			int coreDistantNode=r.getEdge().getCoreness1();
	
			if(r.getEdge().getType()==1)
			{
				if(distantNode==r.getStartNode())
				{
					distantNode=r.getEdge().getNode2();
					localNode= r.getEdge().getNode1();
					part.addEdgeAkka(r.getEdge().getNode1(), r.getEdge().getNode2(), r.getEdge().getPartition1(), r.getEdge().getPartition2(), r.getEdge().getCoreness1(), r.getEdge().getCoreness2());
					coreDistantNode=r.getEdge().getCoreness2();
					
					//update the partition identifier of the distant node
					part.distNeighbPartition.put(r.getEdge().getNode2(), r.getEdge().getPartition2());
				}
				else
				{
					part.addEdgeAkka(r.getEdge().getNode2(), r.getEdge().getNode1(), r.getEdge().getPartition2(), r.getEdge().getPartition1(), r.getEdge().getCoreness2(), r.getEdge().getCoreness1());
					//update the partition identifier of the distant node
					part.distNeighbPartition.put(r.getEdge().getNode1(), r.getEdge().getPartition1());
				}
									
				if(!part.frontierNodes.contains(distantNode))
				{
					part.frontierNodes.addElement(distantNode);
				}
	
				
				//update the coreness of the distant node
				part.distNeighbCoreness.put(distantNode,coreDistantNode );
			}
			else
			{
				part.removeEdge(r.getEdge().getNode1(), r.getEdge().getNode2());
			}
			
			part.inducedCoreSubgraphAkka(r.getStartNode()); 
			
			//Add the current node to list of propagated nodes			
			Worker.propagatedNodes.addElement(r.getEdge().getNode1());
			Worker.propagatedNodes.addElement(r.getEdge().getNode2());
						
			Hashtable h= new Hashtable<Integer,IntArray>();
			Hashtable hc= new Hashtable<Integer,Integer>();
			
		    HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase2Msg>> listOfMessage2 = new HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase2Msg>>();

			initListOfMessage2(listOfMessage2);
			int countWorkers=0; 

			Iterator it = part.reachableNodes.iterator();
			while(it.hasNext())
			{
				int elm =(int) it.next();
				if(!part.frontierNodes.contains(elm))
				{
					hc.put(elm, part.distNeighbCoreness.get(elm));
					IntArray ia=part.neighborsAkka(elm);
						h.put(elm, ia);
						
						for(int i=0;i<ia.size();i++)
						{
							if(part.distNeighbCoreness.containsKey(ia.get(i)))
							{
									hc.put(ia.get(i), part.distNeighbCoreness.get(ia.get(i)));
							}
						}
				}
				
				else if (!Worker.propagatedNodes.contains(elm))
				{		
					Worker.propagatedNodes.addElement(elm);	
					countWorkers++;
					PropagateReachableNodesW2WCase2Msg workMsg= new PropagateReachableNodesW2WCase2Msg(elm);
					listOfMessage2.get(part.distNeighbPartition.get(elm)).add(workMsg);					
				}
			}
			
			
			if(countWorkers>0)
			{
				ActorRef master=InfoWorkersInWorker.get(0);
				Future<Object> future1 = Patterns.ask(master, new IncrementWorkersMsg(countWorkers), timeout);
				Integer result1 = (int) Await.result(future1, timeout.duration());
	
				ActorRef a=null;
				for (int i=1;i<=InfoWorkersInWorker.size();i++)
				{
					if(listOfMessage2.containsKey(i))
					{
						for (int j=0;j<listOfMessage2.get(i).size();j++)
						{
							PropagateReachableNodesW2WCase2Msg prop = listOfMessage2.get(i).get(j);
							
							a=InfoWorkersInWorker.get(part.distNeighbPartition.get(prop.getNode()));
	
							a.tell(prop, getSelf());
						}
					}
				}
			}
			
			
			
			HashSet tmpReachableNodes= new HashSet(part.reachableNodes); 

			GetReachableNodesMsg res=new GetReachableNodesMsg(tmpReachableNodes,h,hc);
			
			//send the intermediate result to the master
			getSender().tell(res, getSelf());
			
			part.reachableNodes.clear();

		}
		
		// Step 3: Updating the coreness of eligible nodes in the current partition
		else if (message instanceof DistantUpdateMsg) 
		{
					Worker.propagatedNodes.removeAllElements();
					DistantUpdateMsg upmsg = (DistantUpdateMsg) message;
					Hashtable h=(Hashtable)upmsg.getDistCoreness();
					Enumeration hkeys=h.keys();
					while(hkeys.hasMoreElements())
					{
						int key=(int) hkeys.nextElement();
						
						if((part.frontierNodes.contains(key))||(part.distNeighbCoreness.containsKey(key)))
						{
							
							if(upmsg.getTypeEdge()==1)
							{
								part.distNeighbCoreness.put(key, (int)h.get(key)+1);
							}
							else
							{
								part.distNeighbCoreness.put(key, (int)h.get(key)-1);
							}
							
						}
					}
					
					part.intialize();
					
					upmsg.getMasterActor().tell(new FinishUpdateMsg(), getSelf());
		}
		else 
		{
			unhandled(message);
		}
	}
	
	public void initListOfMessage1(HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase1Msg>> list)
	{
		for(int i=1;i<=InfoWorkersInWorker.size();i++)
		{
			list.put(i, new ArrayList<PropagateReachableNodesW2WCase1Msg>());
		}
	}
	public void initListOfMessage2(HashMap<Integer, ArrayList<PropagateReachableNodesW2WCase2Msg>> list)
	{
		for(int i=1;i<=InfoWorkersInWorker.size();i++)
		{
			list.put(i, new ArrayList<PropagateReachableNodesW2WCase2Msg>());
		}
	}
private void doComputation(int step, int partition, int nbNodes) throws Exception {
	part= DistKCore.computekCoreOnPartition("Part_"+partition, "bindata_p"+partition, nbNodes);
}
}
