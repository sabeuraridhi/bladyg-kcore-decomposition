package structure;

import java.io.Serializable;

public final class IntArray  implements Serializable
{

private int[] A;
private int size;

public IntArray(int capacity)
{
	size=0;
	A = new int[capacity];
}

public Object clone()
{
	IntArray temp = new IntArray(size);
	temp.size = size;
	for (int i=0; i < size; i++) {
		temp.A[i] = A[i];
	}
	return temp;
}

public void removeNeighbor(int neigh)
{
	IntArray temp = new IntArray(size);
	temp.size = size;
	int ind=-1;
	for (int i=0; i < size; i++) {
		if(A[i]==neigh)
		{
			ind=i;
			//System.out.println("trouuvee a ind "+ind);
			break;
		}
	}
	if(ind!=-1)
	{
		for (int i=ind; i < size-1; i++) {
			A[i] = A[i+1];
		}
		
		size--;
	}
	
	
}



public boolean contains(int v)
{
	for (int i=0; i < size; i++) {
		if (A[i] == v) {
			return true;
		}
	}
	return false;
}

public void append(int v) 
{
  if (size == A.length) {
  	  A = ArrayUtil.resizeArray(A, A.length, A.length*2);
  }
  A[size] = v;
  size++;
}
public static IntArray fusion(IntArray i1,IntArray i2){
	if(i1==null){
		//System.out.println(" Niil i1");
		return i2;
	}
	else if (i2==null)
	{
		//System.out.println(" Niil i2");

		return i1;
	}
	else
	{
		for(int i=0;i<i1.size();i++)
		{
			if(!i2.contains(i1.get(i)))
			{
				i2.append(i1.get(i));
			}
			else
			{
				
			}
			//System.out.println(" Neighbor of Node "+(int)part.reachableNodes.elementAt(j) +" is " + i1.get(i)); 
		}
		return i2;
	}

}
public int get(int i) 
{
	return A[i];
}

public int size()
{
	return size;
}

public void reset()
{
	size = 0;
}


}
