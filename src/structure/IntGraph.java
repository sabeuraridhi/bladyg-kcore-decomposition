package structure;
import java.util.*;

import akka.actor.ActorPath;

public class IntGraph
{
	private int size; 
	public Vector frontierNodes; 	
	private Hashtable <Integer, IntArray> neighborsHash;
	public  HashSet reachableNodes; 
	private  HashSet visitedNodes; 
	private  Vector GraphNodes;
	//A hashtable containing the coreness of all nodes of the current partition and the frontier nodes
	public Hashtable<Integer, Integer>  distNeighbCoreness;
	//A hashtable containing the identifier of all frontier nodes
	public Hashtable<Integer, Integer>  distNeighbPartition; 
	private int partition;
	//private int[] neighborsPart;
	//private IntArray[] neighbors;
	//public int[] CorenessTable; 


public IntGraph(int size,int part)
{
	this.size = size;	
	visitedNodes = new HashSet();
	reachableNodes = new HashSet();
	frontierNodes = new Vector();
	GraphNodes = new Vector();
	this.partition=part;
	distNeighbCoreness = new Hashtable<Integer, Integer>();
	distNeighbPartition= new Hashtable<Integer, Integer>();
	neighborsHash = new Hashtable<Integer, IntArray>();
	//neighborsPart = new int[size];
	//CorenessTable = new int[size];
	//neighbors = new IntArray[size];

}

public void intialize()
{
	visitedNodes.clear();
	reachableNodes.clear();
	//reachableNodes.removeAllElements();
}
public int size()
{
	return size;
}


//set the initial coreness of nodes in this partition
public void initialcoreness(int[] tab)
{
	for(int i=0;i<GraphNodes.size();i++)
	{
		//System.out.println("Coreness of node "+i+" updated");
		//CorenessTable[i]=tab[i];
		//if(tab[i]!=0)
		//{
			distNeighbCoreness.put((Integer) GraphNodes.elementAt(i), tab[i]);
			//CorenessTable[i]=tab[i];
		//}
	}
}
public void initialcoreness(Hashtable tab)
{
	for(int i=0;i<size;i++)
	{
		//System.out.println("Coreness of node "+i+" updated");
		//CorenessTable[i]=tab[i];
		//if(tab[i]!=0)
		//{
			distNeighbCoreness.put(i, (int)tab.get(i));
			//CorenessTable[i]=tab[i];
		//}
	}
}
/*
//update the coreness of candidate nodes in this partition
public Vector pruneCandidateNodesAkka(Vector cand, Hashtable h, Hashtable hc)
{
	//System.out.println(" Pruning step: ");

	int flag=0;
	//System.out.println("test");
	if(cand ==null)
	{
		return null;
	}
	int count=0;	

	
	for(int i=0;i<cand.size();i++)
	{
		System.out.println(" ***** Candidate node: "+cand.elementAt(i));
		count=0;
		//System.out.println("Node "+(int)cand.elementAt(i));
		//IntArray ng = neighbors((int)cand.elementAt(i)); 
		IntArray ng = (IntArray) h.get(cand.elementAt(i));
		//System.out.println("Number of neighbors of Node: "+(int)cand.elementAt(i)+" is "+ng.size());
		
		for(int j=0;j<ng.size();j++)
		{
			//System.out.println("Verify neighbor "+ng.get(j)+"("+distNeighbCoreness.get(ng.get(j))+") from "+ng.size() +" neighbors ");
			System.out.println("[hc] Verify neighbor "+ng.get(j)+"("+(int)hc.get(ng.get(j))+") from "+ng.size() +" neighbors ");
			//System.out.println("Partition of neighbor "+ng.get(j)+" is "+neighborsPart[ng.get(j)]);
			if(cand.contains(ng.get(j)))
			{
				System.out.println("count++ => Node "+ng.get(j)+" in the candidate set  ");
				count++;				
			}
			//else if ((distNeighbCoreness.get(ng.get(j))>distNeighbCoreness.get((int)cand.elementAt(i))))
			
			else if ((int)hc.get(ng.get(j))>(int)hc.get((int)cand.elementAt(i)))
				{
					//System.out.println("Coreness of node "+ng.get(j)+" ("+distNeighbCoreness.get(ng.get(j))+") is greater than "+ "the coreness of node "+i+" ("+distNeighbCoreness.get(i));
					System.out.println("count++ => Coreness of node "+ng.get(j)+" ("+(int)hc.get(ng.get(j))+") is greater than "+ "the coreness of node "+j+" ("+(int)hc.get((int)cand.elementAt(i)));
					count++;	
				}
			//if(count<=distNeighbCoreness.get((int)cand.elementAt(i))){
			//if(hc.get((int)cand.elementAt(i))==null){System.out.println("Null hc de "+(int)cand.elementAt(i));}
			
		}		
				
			if(count<=(int)hc.get((int)cand.elementAt(i))){
			
			System.out.println("count is less than "+(int)hc.get((int)cand.elementAt(i))+" => Node "+cand.elementAt(i)+" will not be updated");
			cand.removeElementAt(i);
			
			flag=1;
			//CorenessTable[(int)cand.elementAt(i)]++;
			//System.out.println("Coreness of node "+(int)cand.elementAt(i)+" updated ="+CorenessTable[i]);
		}	
	}
	if(flag==1)
		{
		pruneCandidateNodesAkka(cand,h,hc);
		}
	return cand;
}

public void AfficherpruneCandidateNodesAkka(Vector cand, Hashtable h, Hashtable hc)
{
	
	for(int i=0;i<cand.size();i++)
	{
		System.out.println(" [Test] ***** Candidate node: "+cand.elementAt(i));
		
		IntArray ng = (IntArray) h.get(cand.elementAt(i));
		//System.out.println("Number of neighbors of Node: "+(int)cand.elementAt(i)+" is "+ng.size());
		
		for(int j=0;j<ng.size();j++)
		{
			//System.out.println("Verify neighbor "+ng.get(j)+"("+distNeighbCoreness.get(ng.get(j))+") from "+ng.size() +" neighbors ");
			System.out.println("[Test] Node "+ng.get(j)+"is a neighbor with coreness ="+(int)hc.get(ng.get(j))+" from "+ng.size() +" neighbors ");
		}		
	}
}

//update the coreness of candidate nodes in this partition
public Vector pruneCandidateNodesAkka(Vector cand)
{
	int flag=0;
	if(cand ==null)
	{
		return null;
	}
	int count=0;	

	
	
	for(int i=0;i<cand.size();i++)
	{
		//System.out.println(" ***** Candidate node: "+cand.elementAt(i));
		count=0;
		//System.out.println("Node "+(int)cand.elementAt(i));
		IntArray ng = neighbors((int)cand.elementAt(i)); 
		//IntArray ng = (IntArray) h.get(cand.elementAt(i));
		//System.out.println("Number of neighbors of Node: "+(int)cand.elementAt(i)+" is "+ng.size());
		
		for(int j=0;j<ng.size();j++)
		{
			//System.out.println("Verify neighbor "+ng.get(j)+"("+distNeighbCoreness.get(ng.get(j))+") from "+ng.size() +" neighbors "+distNeighbCoreness.get(cand.elementAt(i)));
			//System.out.println("Verify neighbor "+ng.get(j)+"("+(int)hc.get(ng.get(j))+") from "+ng.size() +" neighbors ");
			//System.out.println("Partition of neighbor "+ng.get(j)+" is "+neighborsPart[ng.get(j)]);
			if(cand.contains(ng.get(j)))
				{
				//System.out.println("Node "+ng.get(j)+" is in the candidate set ");

				count++;				
				}
			else if ((distNeighbCoreness.get(ng.get(j))>distNeighbCoreness.get((int)cand.elementAt(i))))
			//else if ((int)hc.get(ng.get(j))>(int)hc.get((int)cand.elementAt(i)))
				{
					//System.out.println("The coreness of node "+ng.get(j)+" ("+distNeighbCoreness.get(ng.get(j))+") is greater than "+ "the coreness of node "+(int)cand.elementAt(i)+" ("+distNeighbCoreness.get((int)cand.elementAt(i)));
					//System.out.println("Coreness of node "+ng.get(j)+" ("+(int)hc.get(ng.get(j))+") is greater than "+ "the coreness of node "+i+" ("+(int)hc.get(i));

					count++;	
				}
		}		
		if(count<=distNeighbCoreness.get((int)cand.elementAt(i))){
			//System.out.println("Node "+cand.elementAt(i)+" will not be updated");
			cand.removeElementAt(i);
			
			flag=1;
			//CorenessTable[(int)cand.elementAt(i)]++;
			//System.out.println("Coreness of node "+(int)cand.elementAt(i)+" updated ="+CorenessTable[i]);
		}	
	}
	if(flag==1)
		{
			pruneCandidateNodesAkka(cand);
		}
	return cand;
}
*/

public void updateCoreness(Vector v)
{
	for(int i=0;i<v.size();i++)
	{
		//CorenessTable[(int)v.elementAt(i)]++;
		int core=distNeighbCoreness.get((int)v.elementAt(i));
		distNeighbCoreness.put((int)v.elementAt(i), core+1);
		System.out.println("Coreness of node "+(int)v.elementAt(i)+" updated =>"+distNeighbCoreness.get((int)v.elementAt(i)));
	}
}
public void updateCorenessAkka(Vector v, Hashtable h, Hashtable hc)
{
	for(int i=0;i<v.size();i++)
	{
		//CorenessTable[(int)v.elementAt(i)]++;
		//int core=distNeighbCoreness.get((int)v.elementAt(i));
		int core=(int) hc.get((int)v.elementAt(i));
		distNeighbCoreness.put((int)v.elementAt(i), core+1);
		System.out.println("Coreness of node "+(int)v.elementAt(i)+" updated =>"+distNeighbCoreness.get((int)v.elementAt(i)));
	}
}

public void inducedCoreSubgraphAkka(int node)
{
	//intialize();
	//System.out.println("Coreness of node "+1+" = "+distNeighbCoreness.get(1)); 
	int coreness=distNeighbCoreness.get(node); 
	IntArray inducedCS = notVisitedNeighbors(node); 
	
	visitedNodes.add(node);
	reachableNodes.add(node);

	if(inducedCS.size()==0)
	{
		//System.out.println("null");
		return ;
	}
	else
	{
		for(int i=0;i<inducedCS.size();i++)
		{
			//System.out.println("node "+inducedCS.get(i)+" is neighbor of node"+ node);
			visitedNodes.add(inducedCS.get(i));
			if(!distNeighbCoreness.containsKey(inducedCS.get(i)))
				distNeighbCoreness.put(inducedCS.get(i), 0);
			
			
			if (distNeighbCoreness.get(inducedCS.get(i))==coreness)
			{
				//if (distNeighbCoreness.get(inducedCS.get(i))==coreness)
				//{
					//System.out.println("node "+inducedCS.get(i)+" is part of the induced core subgraph");
					//if(!reachableNodes.contains(inducedCS.get(i)))
					//	reachableNodes.addElement(inducedCS.get(i));
					inducedCoreSubgraphAkka(inducedCS.get(i));
				//}
			}
			/*
			else
			{
				distNeighbCoreness.put(inducedCS.get(i), 0);

			}
			*/
		}
	}

}


public IntArray neighbors(int i)
{
	if(!neighborsHash.containsKey(i))
	{
		neighborsHash.put(i, new IntArray(4));
	}
	
	return neighborsHash.get(i);
	
	/*
	if (neighbors[i] == null) 
	{
		//System.out.println("neighbor"+i+"does not exist");
		neighbors[i] = new IntArray(4);
	}
	return neighbors[i];
	*/
}

public IntArray neighborsAkka(int i)
{
	

	if(!neighborsHash.containsKey(i))
	{
		neighborsHash.put(i, new IntArray(4));
	}
	
	return neighborsHash.get(i);
	
	/*
	if (neighbors[i] == null) 
	{
		return null;
	}
	return neighbors[i];
	*/
}

public IntArray notVisitedNeighbors(int i)
{
	IntArray nvn=new IntArray(4);
	
	/*
	if(!neighborsHash.containsKey(i))
	{
		neighborsHash.put(i, new IntArray(4));
	}
	*/
	
	IntArray ng=neighborsHash.get(i);
	/*
	if (neighbors[i] == null) 
	{
		neighbors[i] = new IntArray(4);
	}
	
	IntArray ng=neighbors[i];
	
	*/
	if(ng!=null)
	{
		for(int j=0;j<ng.size();j++)
		{
			if(!visitedNodes.contains(ng.get(j))){
				nvn.append(ng.get(j));
			}
			
		}
	}
	else
	{
		neighborsHash.put(i, new IntArray(4));

	}
	return nvn;
}


public void addEdge(int i, int j)
{
	
	
	if(!GraphNodes.contains(i))
	{
		GraphNodes.addElement(i);
	}
	if(!GraphNodes.contains(j))
	{
		GraphNodes.addElement(j);
	}
	
	if(!neighborsHash.containsKey(i))
	{
		neighborsHash.put(i, new IntArray(4));
	}
	
	if(!neighborsHash.get(i).contains(j))
	{
		//System.out.println("Neighbor "+j+" added");
		IntArray ia=neighborsHash.get(i);
		ia.append(j);
		neighborsHash.put(i, ia);
	}
	/*
	if (neighbors[i] == null) 
		neighbors[i] = new IntArray(4);
	if (!neighbors[i].contains(j))
		neighbors[i].append(j);
		
	*/
}
public void removeEdge(int i, int j)
{
	IntArray neighbors1=neighbors(i);
	IntArray neighbors2=neighbors(j);
	//System.out.println("Removing neighbor "+i+" from "+j);
	//System.out.println("Removing neighbor "+j+" from "+i);
	neighbors1.removeNeighbor(j);
	neighbors2.removeNeighbor(i);	
	//IntArray neighbors3=neighbors(i);
	//IntArray neighbors4=neighbors(j);
}
public void addEdgeAkka(int i, int j, int p1, int p2, int c1,int c2)
{
	if(!GraphNodes.contains(i))
	{
		GraphNodes.addElement(i);
	}
	if(!GraphNodes.contains(j))
	{
		GraphNodes.addElement(j);
	}
	
	
	if(!neighborsHash.containsKey(i))
	{
		neighborsHash.put(i, new IntArray(4));
	}
	if(!neighborsHash.get(i).contains(j))
	{
		neighborsHash.get(i).append(j);
	}
	
	/*
	if (neighbors[i] == null) 
		neighbors[i] = new IntArray(4);
	if (!neighbors[i].contains(j))
	{
		neighbors[i].append(j);
		
	}
	*/
}
public String toString(){
	for(int i=0;i<size;i++)
	{
		IntArray ia=neighbors(i);
		for(int j=0;j<ia.size();j++)
		System.out.println("("+i+","+ia.get(j)+")");
	}
	return "" ;
}

public Vector getGraphNodes() {
	return GraphNodes;
}

public void setGraphNodes(Vector graphNodes) {
	GraphNodes = graphNodes;
}

public int getSize() {
	return size;
}

public void setSize(int size) {
	this.size = size;
}
	
}


//candidate nodes
/*
public Vector toBeUpdatedNodes(Edge e)
{
	if(e==null){
		return null;
	}
	//get the partition of the distant node
	int part=e.partition2;
	Vector v1,v2;
	//get the coreness of the distant node
	int corenessNode2=e.coreness2;
	if (CorenessTable[e.node1]<CorenessTable[e.node2])
	{
		//System.out.println("Candidate nodes are nodes of the induced core subgraph from node "+e.node1);
		inducedCoreSubgraph(e.node1);


		//Looking for the set of nodes that need to be updated
		Vector toBeUpdated=pruneCandidateNodes(reachableNodes);
		
		return toBeUpdated;
		
	}
	else if (CorenessTable[e.node1]>CorenessTable[e.node2])
	{
		//System.out.println("candidate nodes are nodes of the induced core subgraph from node "+e.node2);
		//System.out.println("Remote call to partition "+part+" in order to look for candidate nodes");
		inducedCoreSubgraph(e.node2);


		//Looking for the set of nodes that need to be updated
		Vector toBeUpdated=pruneCandidateNodes(reachableNodes);
				
		return toBeUpdated;
	}
	else
	{
		//System.out.println("candidate nodes are union of the induced core subgraph from node "+e.node1+" the induced core subgraph from node "+e.node2);
		//System.out.println("Local call to partition "+e.partition1+" in order to look for candidate nodes");
		
		//System.out.println("Remote call to partition "+part+" in order to look for candidate nodes");
		
		inducedCoreSubgraph(e.node1);
		inducedCoreSubgraph(e.node2);
		
		
		
		//Looking for the set of nodes that need to be updated
				Vector toBeUpdated=pruneCandidateNodes(reachableNodes);
				
				return toBeUpdated;
	}
	
}
*/

/*
//update the coreness of candidate nodes in this partition
public Vector pruneCandidateNodes(Vector cand)
{
	int flag=0;
	if(cand ==null)
	{
		return null;
	}
int count=0;	

	
	for(int i=0;i<cand.size();i++)
	{
		count=0;
		//System.out.println("Node "+(int)cand.elementAt(i));
		IntArray ng = neighbors((int)cand.elementAt(i)); 
		for(int j=0;j<ng.size();j++)
		{
			//System.out.println("Verify neighbor "+ng.get(j)+"("+CorenessTable[ng.get(j)]+") from "+ng.size() +" neighbors "+CorenessTable[(int)cand.elementAt(i)]);
			if(cand.contains(ng.get(j)))
				{
				//System.out.println("node "+ng.get(j)+" in the candidate set ");

				count++;				
				}
				else if ((CorenessTable[ng.get(j)]>CorenessTable[(int)cand.elementAt(i)]))
				{
					//System.out.println("Coreness of node "+ng.get(j)+" ("+CorenessTable[ng.get(j)]+") is greater than "+ "the coreness of node "+i+" ("+CorenessTable[i]);

					count++;	
				}
		}		
		if(count<=CorenessTable[(int)cand.elementAt(i)]){
			//System.out.println("Node "+cand.elementAt(i)+" will not be updated");
			cand.removeElementAt(i);
			
			flag=1;
			//CorenessTable[(int)cand.elementAt(i)]++;
			//System.out.println("Coreness of node "+(int)cand.elementAt(i)+" updated ="+CorenessTable[i]);
		
		
		}	
	}
	if(flag==1)
		{
		pruneCandidateNodes(cand);
		}
	return cand;
}

*/

/*
//determine the induced core subgraph from a node
public void inducedCoreSubgraph(int node)
{
	int coreness=CorenessTable[node]; 
	IntArray inducedCS = notVisitedNeighbors(node); 
	
	visitedNodes.add(node);
	if(!reachableNodes.contains(node))
		reachableNodes.addElement(node);
	if(inducedCS.size()==0)
	{
		//System.out.println("null");
		return ;
	}
	else
	{
		for(int i=0;i<inducedCS.size();i++)
		{
			//System.out.println("node "+inducedCS.get(i)+" is neighbor of node"+ node);
			visitedNodes.addElement(inducedCS.get(i));
			if (CorenessTable[inducedCS.get(i)]==coreness)
			{
				//System.out.println("node "+inducedCS.get(i)+" is part of the induced core subgraph");
				if(!reachableNodes.contains(inducedCS.get(i)))
					reachableNodes.addElement(inducedCS.get(i));
			
				inducedCoreSubgraph(inducedCS.get(i));
			}
			
		}
	}

}
*/