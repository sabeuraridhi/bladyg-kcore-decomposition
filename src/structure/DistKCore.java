package structure;
import graphTools.GraphTools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Timer;
import java.util.Vector;
import java.util.concurrent.TimeUnit;


public class DistKCore {

	public static void test(String file) throws Exception
	{
		String line;
	
		BufferedReader reader = new BufferedReader(new FileReader(file));
		line=reader.readLine();line=reader.readLine();line=reader.readLine();
		while((line=reader.readLine())!=null)
		{
			String[] parts = line.split("\\s");
			System.out.println("Edge ("+parts[0]+","+parts[1]+")");
		}
		reader.close();
	}
	
	
	public static IntGraph graphConstruction(String file, int nbNodes) throws IOException
	{
		int size=nbNodes;
		//int startFrom;
		String line;
		int counter=0;
		BufferedReader reader = new BufferedReader(new FileReader(file));

		//System.out.println("Graph Construction ....");
		IntGraph gr = new IntGraph(size,1);
		while((line=reader.readLine())!=null)
		{
			counter++;
			String[] parts = line.split("\\s");
			//System.out.println("Edge ("+parts[0]+","+parts[1]+")");
			gr.addEdge(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
			gr.addEdge(Integer.parseInt(parts[1]), Integer.parseInt(parts[0]));
		}
		//gr.setSize(gr.getGraphNodes().size());
		reader.close();
		
		return gr;
	}
	
	public static void graphFileConstruction(IntGraph graph, String filename, int nbNodes) throws Exception{
			
		DataOutputStream out1 = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filename)));

		int size1=graph.size(); //number of nodes of graph 1
		int size2=graph.getGraphNodes().size(); 
		 
		out1.writeInt(size2);
		for (int i=0; i < size2; i++) {
			out1.writeInt(i);
			//out1.writeInt((int) graph.getGraphNodes().elementAt(i));
			int degree = graph.neighbors((int) graph.getGraphNodes().elementAt(i)).size();
			out1.writeInt(degree);
			
			for (int j=0; j < degree; j++) {
				out1.writeInt((int) graph.getGraphNodes().indexOf(graph.neighbors((int) graph.getGraphNodes().elementAt(i)).get(j)));
				
			}
		}
		out1.close();
	
	}
	public static IntGraph computekCoreOnPartition(String originalFile, String graphFile, int nbNodes) throws Exception
	{
		//transform the original dataset file to a graph object IntGraph
		System.out.println("Graph Construction ....");
		IntGraph part1 = DistKCore.graphConstruction(originalFile, nbNodes);
		//transform the graph object IntGraph to a binary file
		//System.out.println("Graph File Construction ....");
		DistKCore.graphFileConstruction(part1,graphFile, nbNodes);
	    
		//Pseudocode of our approach 
				
		//step 1: execution of the standard k-core decomposition algorithm in parallel
	    //System.out.println("****** Step 1: Computing the coreness in local partitions ******");
		System.out.println("k-cores computation ....");
		KShellFS ks = new KShellFS();
		int[] corenessTable1=ks.execute(graphFile);
		part1.initialcoreness(corenessTable1);
		
		//System.out.println("max k computation ....");
		int max=corenessTable1[0];
		for(int j=0;j<corenessTable1.length;j++)
		{
			if(corenessTable1[j]> max){
				max = corenessTable1[j];
			}
			
		}
		
		System.out.println("Step 1: Done!");
		//System.out.println(" Max k = "+max); 
		return part1;
	}
	public static void centralizedApproach(String originalFile, String graphFile, Vector frontierEdges, int nbNodes) throws Exception
	{
		//transform the original dataset file to a graph object IntGraph
		IntGraph part1 = DistKCore.graphConstruction(originalFile,nbNodes);
		//transform the graph object IntGraph to a binary file 
		DistKCore.graphFileConstruction(part1,graphFile,nbNodes);
	    
		//Pseudocode of our approach 
				
		//step 1: execution of the standard k-core decomposition algorithm in parallel
	    System.out.println("****** Step 1: Computing the coreness in local partitions ******");
		KShellFS ks = new KShellFS();
		int[] corenessTable1=ks.execute(graphFile);
		//part1.initialcoreness(corenessTable1);
		int maxK=0;
		System.out.println("Step 1: Done!");	
		for(int j=0;j<part1.getSize();j++)
		{
			System.out.println(" k("+j+") ="+corenessTable1[j]+" | "); 
			if(corenessTable1[j]>maxK)
			{
				maxK=corenessTable1[j];
			}
		}
		System.out.println(" Max k= "+maxK);
		
		
		
		System.out.println("Baseline algorithm: Done!");
	}

}
